# Orthogonal Ribosomal Flow Model

This code implements an a model of protein translation with orthogonal ribosomes. 


Code is written and tested on MATLAB R2020a.

Numeric and visualization routines are in `src`

Experiments and demonstrations are in `RFM_figures`

Refer to the `RFM_figures` readme for more detail.

## Reference
https://arxiv.org/abs/2009.00539 "Mediating Ribosomal Competition by Splitting Pools"

If you find this work useful, please cite:

```
@misc{miller2020mediating,
      title={Mediating Ribosomal Competition by Splitting Pools}, 
      author={Jared Miller and M. Ali Al-Radhawi and Eduardo D. Sontag},
      year={2020},
      eprint={2009.00539},
      archivePrefix={arXiv},
      primaryClass={q-bio.MN}
}
```

## Contact
For comments and questions please email [Jared Miller](mailto:miller.jare@northeastern.edu?Subject=Orthogonal_Ribosomes).
