G =[  1    -1     0     0     0     0     0     0
    -1     1     0     0     0     0     0     0
     0    -1     1     0     0     0     0     0
     0     1    -1     0     0     0     0     0
     0     0     0     1    -1     0     0     0
     0     0     0    -1     1     0     0     0
     0     0     0     0    -1     1     0     0
     0     0     0     0     1    -1     0     0
    -1     0     1     0     0     0     1    -1
     0     0     0    -1     0     1    -1     1]; % ORFM with two local pools and one global pool. There is one 2-codon mRNA strand per local pool
 
 LEARNmain(G)