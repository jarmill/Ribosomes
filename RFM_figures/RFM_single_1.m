%N_species = 2;
%N_codons = 3;
N_species = 1;
N_codons = 5;
rng(40);
% N_states = N_species * N_codons;

%lambda = ones(N_species, N_codons+1);
%lambda = [1 1 1 1; 2 1 1 1];
%lambda = [1 1 1 1 1 1; 
%          2 2 2 2 2 2];
%o = ones(1, 6);

% o = ones(N_codons+1, 1);
% lam = 2*ones(N_species, 1);
% lambda =lam * o';
% 
% lambda(1, 1) = 3;
% lambda(2, 1) = 2;
% lambda(3, 1) = 1;

lambda  = 3*rand(N_species, N_codons + 1) + 0.5;

%lambda = [4*o ;2*o];
%lambda = 
%lambda = [1 1 1 1; 0 0 0 0];

RFM = RFM_Single_Cross(lambda);

%x0 = zeros(1, N_states) +  0.4;
%x0 = rand(1, N_states);
%x =  repmat([0.5; 0], [N_codons, 1]);
%[flow, xdot]  = RFM.rna_flow(x);

r0 = rand(1, N_codons);
r1 = rand(1, N_codons);
%x0 = reshape([r0; r0] .* [r1; 1-r1], [N_states, 1]);



t_max = 100;
RFM = RFM.simulate(t_max);

%draw RFM

STEADY_STATE = 1;

if STEADY_STATE
    v = RFM_Single_Cross_Visualizer(RFM);
    xss = RFM.traj.x(end, :);
    v.draw(Inf, xss)
%v.playback()
else
    t_test = 2;
    x_test = ppval(RFM.traj.spline, 2);
    v.draw(t_test, x_test)
end
% 
% figure(1)
% clf;
% 
% subplot(2, 1, 1)
% plot(RFM.traj.t, RFM.traj.x(:, 1:2:end));
% 
% subplot(2, 1, 2)
% plot(RFM.traj.t, RFM.traj.x(:, 2:2:end));
%plot(RFM.traj.t, RFM.traj.x);