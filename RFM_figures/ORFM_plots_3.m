%Orthogonal RFM plotter with 2 species
% Fig 3a: demand = 0
% Fig 3b: demand = 2



demand = 2; %0,1,2 for no, low high demand


%general setup
simple_model = 0;
rfm = RFM_Split_Network(simple_model);
N_r = 10;
% t_max = 30;
t_max = 300;


%% add rna to system
%rfm = rfm.pool_rates(0.3, 0.3, 0.3, 0.3);
%rfm = rfm.pool_rates(0.5, 0.5, 0.5, 0.5);
% rfm = rfm.pool_rates(1, 1, 0.5, 0.5);
rfm = rfm.pool_rates(1, 1, 1, 1);
%on the host
Nh1 = 6;
lambdah1 = 3*ones(Nh1+1, 1);
rfm = rfm.add_rna(Nh1, lambdah1, 0);

Nh2 = 5;
lambdah2 = 3*ones(Nh2+1, 1);
lambdah2(1) = 1;
rfm = rfm.add_rna(Nh2, lambdah2, 0);

Nh3 = 3;
lambdah3 = 5*ones(Nh3+1, 1);
rfm = rfm.add_rna(Nh3, lambdah3);

Nh4 = 4;
lambdah4 = 7*ones(Nh4+1, 1);
lambdah4(1:2) = 2;
rfm = rfm.add_rna(Nh4, lambdah4);

if demand > 0
    Nc2 = 4;
    lambdac2 = 5*ones(Nc2 +1, 1);
    lambdac2(3) = 1;
    %lambdac2(4) = 1;
    rfm = rfm.add_rna(Nc2, lambdac2, 1);

    if demand > 1
        Nc3 = 3;
        lambdac3 = 4*ones(Nc3+1, 1);
        rfm = rfm.add_rna(Nc3, lambdac3, 1);

        Nc4 = 4;
        lambdac4 = 1*ones(Nc4+1, 1);
        rfm = rfm.add_rna(Nc4, lambdac4, 1);
        
        Nc5 = 6;
        lambdac5 = 3*ones(Nc5+1, 1);
        rfm = rfm.add_rna(Nc5, lambdac5, 1);
    end
end



%get the party started
rfm = rfm.simulate(N_r, t_max);

%try the visualizer
v = RFM_Split_Visualizer(rfm);
 
x_ss = rfm.find_steady_state(sum(N_r));
% x_ss = rfm.traj.x(end, :)';
y_ss = rfm.output(x_ss);

%v.draw(t_max, x_end)

%x_ss = rfm.find_steady_state(sum(N_r));
v.draw(Inf, x_ss);
% title('')