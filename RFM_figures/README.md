This directory contains demonstrations of the ORFM model. Matlab code and the generated figures are provided.

##Figures in paper

RFM_single_1: A visualization of the steady state of a 5-codon RFM (single strand of mRNA)

RFMNP_2a and RFMNP_2b: Steady state of multiple strands of mRNA taking ribosomes from a common pool (RFM Network with Pools)

ORFM_plots_3_7: Steady states of an Orthogonal Ribosome system with 2 species of ribosomes (host and circuit). 

RFM_multi_species_4: Steady state of a 3-species orthogonal ribosome system

RFM_multi_opt_5_6: A simple example of the optimization routine to maximize total protein throughput by adjusting rates K on a 2-species ORFM.

ORFM_inhibitor_plots_7: ORFM with 2 species that includes an inhibitor (feedback controller) on the circuit.



## Crosstalk: 

Strands of mRNA can accept ribosomes from multiple pools

RFM_Single_Cross_tester: A single strand of mRNA accepting multiple species of ribosomes

RFM_Cross_tester: A 2-species cross-talk ORFM. Animation is in rna_cross_2.mp4



##LEARN: 

The LEARN package (available at https://github.com/malirdwi/LEARN) attempts to find Robust Lyapunov Functions to verify system stability.

LEARN_exampleORFM: Execution of LEARN to verify stability of a 2-species ORFM. There is 1 mRNA with 2 codons per ribosomal species. All rates lambda = 1, and the saturation function is G(z) = z

LEARN_exampleORFM_output: The output trace of LEARN_exampleORFM. Robust Lyapunov Functions could be found, and therefore this ORFM is stable.