%Figure 2a of LCSS-ACC paper
%RFM with bottleneck on first strand

%general setup
rfm = RFM_Network();
N_r = 5;
t_max = 120;

%add rna to system
N1 = 5;
lambda1 = 5*ones(N1+1, 1);
lambda1(3)   = 40;
rfm = rfm.add_rna(1, N1, lambda1);

N2 = 5;
lambda2 = 5*ones(N2+1, 1);
rfm = rfm.add_rna(1, N2, lambda2);

N3 = 5;
lambda3 = 5*ones(N3+1, 1);
rfm = rfm.add_rna(1, N3, lambda3);

%smiulate trajectories
rfm = rfm.simulate(N_r, 10);

%find the steady state
x_ss = rfm.find_steady_state(N_r);

%try the visualizer
v = RFM_Visualizer(rfm);


%x_end = rfm.traj.x(end, :);
v.draw(t_max, x_ss)
