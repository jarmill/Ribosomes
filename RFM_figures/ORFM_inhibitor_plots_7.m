%Orthogonal RFM plotter with 2 species and circuit inhibitor
% Fig 7a: demand = 0
% Fig 7b: demand = 2



HIGH_DEMAND = 0;

if HIGH_DEMAND
    demand = 2;
else
    demand = 0;
end

% demand = 0; %no demand
% demand = 2; %0,1,2 for no, low high demand

%general setup
simple_model = 0;
rfm = RFM_Split_Network(simple_model);
N_r = 10;
% t_max = 30;
t_max = 5000;
% t_max = 2000;


%% add rna to system
%rfm = rfm.pool_rates(0.3, 0.3, 0.3, 0.3);
%rfm = rfm.pool_rates(0.5, 0.5, 0.5, 0.5);
% rfm = rfm.pool_rates(1, 1, 0.5, 0.5);
rfm = rfm.pool_rates(1, 1, 1, 1);
%on the host
Nh1 = 6;
lambdah1 = 3*ones(Nh1+1, 1);
rfm = rfm.add_rna(Nh1, lambdah1, 0);

Nh2 = 5;
lambdah2 = 3*ones(Nh2+1, 1);
lambdah2(1) = 1;
rfm = rfm.add_rna(Nh2, lambdah2, 0);

Nh3 = 3;
lambdah3 = 5*ones(Nh3+1, 1);
rfm = rfm.add_rna(Nh3, lambdah3);

Nh4 = 4;
lambdah4 = 7*ones(Nh4+1, 1);
lambdah4(1:2) = 2;
rfm = rfm.add_rna(Nh4, lambdah4);

%on the circuit
%add an inhibitor
    %(p0, gamma, delta)
%     rfm = rfm.add_inhibitor(4, 3, 0.5);
%     rfm = rfm.add_inhibitor(4, 3, 0.25);
%     rfm = rfm.add_inhibitor(4, 2, 0.2);

%inhibitor parameters
F0 = 1.5;         %nominal protein quantity
gamma = 4;      %hill coefficient
delta = 0.01;    %degradation rate

rfm = rfm.add_inhibitor(F0, gamma, delta);
Nc1 = 2;
lambdac1 = 5*ones(Nc1 +1, 1);
lambdac1(1) = 0.005;
rfm = rfm.add_rna(Nc1, lambdac1, 1);


if demand > 0
    Nc2 = 4;
    lambdac2 = 5*ones(Nc2 +1, 1);
    lambdac2(3) = 1;
    %lambdac2(4) = 1;
    rfm = rfm.add_rna(Nc2, lambdac2, 1);

    if demand > 1
        Nc3 = 3;
        lambdac3 = 4*ones(Nc3+1, 1);
        rfm = rfm.add_rna(Nc3, lambdac3, 1);

        Nc4 = 4;
        lambdac4 = 1*ones(Nc4+1, 1);
        rfm = rfm.add_rna(Nc4, lambdac4, 1);
        
        Nc5 = 6;
        lambdac5 = 3*ones(Nc5+1, 1);
        rfm = rfm.add_rna(Nc5, lambdac5, 1);
    end
end

%add an inhibitor


%get the party started
rfm = rfm.simulate(N_r, t_max);
x_end = rfm.traj.x(end, :)';
y_host_end = rfm.traj.y_host(end, :);
y_circ_end = rfm.traj.y_circ(end, 2:end);
y_inhib_end = rfm.traj.y_circ(end, 1);

%try the visualizer
v = RFM_Split_Visualizer(rfm);
 
% x_ss = rfm.find_steady_state(sum(N_r));
% x_ss = rfm.traj.x(end, :)';
% y_ss = rfm.output(x_ss);

%v.draw(t_max, x_end)

%x_ss = rfm.find_steady_state(sum(N_r));
% v.draw(Inf, x_ss);
v.draw(t_max, x_end);
title('')