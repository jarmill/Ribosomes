--------------------------------
Welcome to LEARN v1.01, Jan 2020
Developed by M. Ali Al-Radhawi malirdwi@{northeastern.edu,mit.edu,gmail.com}

LEARN tries to construct a Robust Lyapunov Function for a given reaction network.
--------------------------------
The network has 10 species and 8 reactions.
The stoichiometric space is 5-dimensional.
The network has a positive vector in the kernel of the stoichiometry matrix, i.e. it has the potential for positive steady states.
The network is conservative. 
The network has no critical siphons. It is structurally persistent. 
--------------------------------
LEARN will check some necessary conditions
Necessary Condition # 1 ....
The critical siphon necessary condition is satisfied.
Necessary Condition # 2 ....
The sign pattern necessary condition is satisfied.
Necessary Condition # 3 ....
The P matrix necessary condition is satisfied. 
--------------------------------
LEARN will search for a PWL RLF
Method # 1: Graphical Method .. 
This is not an M-network. Method # 1 is not applicable. 

--------------------------------
Method # 2: Iterative Method .. 
Success!! A PWL RLF has been found.
The following is always a Lyapunov function for any monotone kinetics: V(x)=|| C*R(x) ||_infty, 
 where C is given as follows:
 1	-1	 0	 0	 0	 0	 0	 0	
 0	-1	 1	 0	 0	 0	 0	 0	
 0	 0	 0	 1	-1	 0	 0	 0	
 0	 0	 0	 0	-1	 1	 0	 0	
-1	 0	 1	 0	 0	 0	 1	-1	
 0	 0	 0	-1	 0	 1	-1	 1	
 0	-1	 1	 0	 0	 0	 1	-1	
 1	 0	-1	 0	 0	 0	 0	 0	
 0	 0	 0	 0	-1	 1	-1	 1	
 0	 0	 0	 1	 0	-1	 0	 0	
-1	 1	 0	 0	 0	 0	 1	-1	
-1	 0	 1	-1	 0	 1	 0	 0	
 0	 0	 0	-1	 1	 0	-1	 1	
 0	 0	 0	 0	 0	 0	 1	-1	
 0	-1	 1	-1	 0	 1	 0	 0	
-1	 0	 1	 0	-1	 1	 0	 0	
-1	 1	 0	-1	 0	 1	 0	 0	
-1	 0	 1	-1	 1	 0	 0	 0	
 0	-1	 1	 0	-1	 1	 0	 0	
 0	-1	 1	-1	 1	 0	 0	 0	
-1	 1	 0	 0	-1	 1	 0	 0	
 0	 1	-1	-1	 0	 1	-1	 1	
-1	 1	 0	-1	 1	 0	 0	 0	
-1	 0	 1	 0	 1	-1	 1	-1	
 0	-1	 1	 0	 1	-1	 1	-1	
 1	 0	-1	-1	 0	 1	-1	 1	
 0	 1	-1	-1	 1	 0	-1	 1	
-1	 1	 0	 0	 1	-1	 1	-1	
 1	-1	 0	-1	 0	 1	-1	 1	
 1	 0	-1	-1	 1	 0	-1	 1	
 1	-1	 0	-1	 1	 0	-1	 1	

The robust non-degeneracy test is passed. 
 Since the network is conservative and with no critical sihpons then the following holds: 
 There exists a unique postive globally asymptotically stable steady state  in each stoichiometric class.
--------------------------------
Method # 3: Linear Programming Method .. 
The partition matrix H is set to the default choice H=the stoichiometry matrix .. 
Success!! A PWL RLF has been found.
The following is always a Lyapunov function for any monotone kinetics: V(x)=|| C*R(x) ||_infty, 
 where C is given as follows:
 0	 1	-1	 0	 1	-1	 0	 0	
 0	 1	-1	-1	 1	 0	-1	 1	
-1	 1	 0	 0	 1	-1	 1	-1	
-1	 1	 0	-1	 1	 0	 0	 0	
 0	 1	-1	 0	 0	 0	 0	 0	
 0	 1	-1	-1	 0	 1	-1	 1	
-1	 1	 0	 0	 0	 0	 1	-1	
-1	 1	 0	-1	 0	 1	 0	 0	
 0	 1	-1	 1	 0	-1	 0	 0	
 0	 1	-1	 0	 0	 0	-1	 1	
-1	 1	 0	 1	 0	-1	 1	-1	
-1	 1	 0	 0	 0	 0	 0	 0	
 0	 1	-1	 1	-1	 0	 0	 0	
 0	 1	-1	 0	-1	 1	-1	 1	
-1	 1	 0	 1	-1	 0	 1	-1	
-1	 1	 0	 0	-1	 1	 0	 0	
 0	 0	 0	 0	 1	-1	 0	 0	
 0	 0	 0	-1	 1	 0	-1	 1	
-1	 0	 1	 0	 1	-1	 1	-1	
-1	 0	 1	-1	 1	 0	 0	 0	
 0	 0	 0	-1	 0	 1	-1	 1	
-1	 0	 1	 0	 0	 0	 1	-1	
-1	 0	 1	-1	 0	 1	 0	 0	
 0	 0	 0	 1	 0	-1	 0	 0	
 0	 0	 0	 0	 0	 0	-1	 1	
-1	 0	 1	 1	 0	-1	 1	-1	
-1	 0	 1	 0	 0	 0	 0	 0	
 0	 0	 0	 1	-1	 0	 0	 0	
 0	 0	 0	 0	-1	 1	-1	 1	
-1	 0	 1	 1	-1	 0	 1	-1	
-1	 0	 1	 0	-1	 1	 0	 0	

The robust non-degeneracy test is passed. 
 Since the network is conservative and with no critical sihpons then the following holds: 
 There exists a unique postive globally asymptotically stable steady state  in each stoichiometric class.
Please note that this function is a Sum-of-Currents RLF which can alternatively be written 
  as V(x)= sum_i xi_i |dot x_i|, where xi=[xi_1 .... xi_n]=   
 1	 1	 1	 1	 1	 1	 1	 1	 2	 2	
THE END.