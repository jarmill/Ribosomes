%general setup
N_species = 2;
rfm = RFM_Multi_Network(N_species);
rfm0 = cell(N_species, 1);

for i = 1:N_species
    rfm0{i} = RFM_Network();
end

%N_r = 3.25;
%N_r = 6;
% N_r = 10;
N_r = 15;
% N_r = 20;
%N_r =  [10, 20, 0, 10];
%N_r = [12, 3, 5];
%t_max = 120;
%t_max = 300;
%t_max = 30;
t_max = 60;
%t_max = 20;
%t_max = 30;
% N_codon_max = 8;
N_codon_max = 6;
N_strand_max = 6;


%% add rna to system
%rfm = rfm.pool_rates(0.3, 0.3, 0.3, 0.3);
%rfm = rfm.pool_rates(0.5, 0.5, 0.5, 0.5);
%tag_rate = [1, 3, 2];
%untag_rate = [0.5, 0.5, 0.5];

tag_rate   = 0.1*ones(N_species, 1);
untag_rate = 0.1*ones(N_species, 1);

%tag_rate = [1, 1,1];
%untag_rate = [1,1,1];


rfm = rfm.pool_rates(tag_rate, untag_rate);
%rfm = rfm.pool_rates(1, 1, 0.5, 0.5);

rng('default')
%rng(2)
rng(29)
mult = 1;
rand_strands = true;
for s = 1:N_species
    if rand_strands 
        num_strand = randi(N_strand_max);
    else
        num_strand = 1;
    end
    %num_strand = 1;
    for i = 1:num_strand
        N_codon = randi(N_codon_max-1)+1;
        mult = 1;
        
        %lambda = random('gamma', 2, 1, [N_codon+1, 1]);
        lambda = 3*rand(N_codon+1, 1) + 0.5;
        %rfm = rfm.add_rna(N_codon, lambda, s, mult);
        rfm = rfm.add_rna(mult, lambda, s);
        rfm0{s} = rfm0{s}.add_rna(mult, N_codon,lambda);
    end
end  

%try the visualizer
DRAW = 1;
OPT  = 1;
OPT_LANDSCAPE = 0;

if OPT
    %total protein production
    w = ones(sum(rfm.N_strands), 1);
    f = @(K1, K2) rfm.steady_state_output(w, N_r, [K1; K2], 1);
    flog = @(lK1, lK2) f(10^K1, 10^K2);
    
    
    %don't adjust any more parameters
    Kmax = 5;
    Kmin = 1/Kmax;
    %objlog2 = @(lK2) -f(Kmax, 10^lK2);
    obj2 = @(K2) -f(Kmax, K2);
    [K2_opt, y_K2_opt] = fminbnd(obj2, Kmin, Kmax);
    y_K2_opt = -y_K2_opt;
%     [K2_opt, y_K2_opt] = fminbnd(objlog2, log10(Kmin), log10(Kmax));
    

    %evaluate y at every K point
    
end

if DRAW    
    %all 1
%     rfm = rfm.pool_rates(tag_rate, untag_rate);
    
    %all 5
%     rfm = rfm.pool_rates(5*tag_rate, untag_rate);
    
    %optimal
    rfm = rfm.pool_rates([5; 0.7354].* tag_rate, untag_rate);

    x_ss = rfm.find_steady_state(sum(N_r));
    y_w_ss = sum(rfm.output(x_ss));
    f(5, 0.7354)
%     f(1, 1)
    %f(5,5)
    %  
    v = RFM_Multi_Visualizer(rfm);
    v.draw(Inf, x_ss);
end

%optimal output

if OPT_LANDSCAPE
    NK = 40;
    Krange = logspace(log10(Kmin), log10(Kmax), NK);
    [K1, K2] = meshgrid(Krange);
    [LK1, LK2] = meshgrid(log10(Krange));
    
%     y_eval = arrayfun(f, K1, K2);
    
    figure(2)
    clf
    hold on
    surf(K1, K2, y_eval)
    %surf(10.^K1, 10.^K2, f_rib)
    %xlabel('log_{10}(K_1)')
    %ylabel('log_{10}(K_2)')
    xlabel('K_1')
    ylabel('K_2')
    zlabel('y_W')
    %scatter3(Kmax, K2_opt, y_K2_opt, [], 100, '*r', 'LineWidth')

    set(gca, 'Xscale', 'log')
    set(gca, 'Yscale', 'log')
    title('weighted protein output vs. pool rates')
    %scatter3(log10(K_opt(1)), log10(K_opt(2)), y_opt, [], 20, '*r')
    view(3)
    hold off
    
    figure(3)
    clf
    hold on
    [cs, h] = contour(LK1, LK2, y_eval, 20, 'ShowText','on', 'LineWidth', 2);
    clabel(cs,h);
    h.LevelList = round(h.LevelList, 2);
    xlabel('Log_{10}(K_1)')
    ylabel('Log_{10}(K_2)')
    scatter(log10(Kmax), log10(K2_opt), 300, '*k', 'LineWidth', 3)
    text(log10(Kmax)-0.03, log10(K2_opt) - 0.07, num2str(y_K2_opt, 3))
    hold off
    axis square
   
end

% %plot contours
% if OPT_LANDSCAPE
% N_range = 20;
% % [K1, K2] = meshgrid(linspace(K_range(1, 1), K_range(1,2), N_range), ...
% %     linspace(K_range(2, 1), K_range(2,2), N_range));
%    
% log_K = 0;
% 
% if ~log_K
% %     [K1, K2] = meshgrid(linspace(K_range(1, 1), K_range(1,2), N_range), ...
% %         linspace(K_range(2, 1), K_range(2,2), N_range));
% 
%     [K1, K2] = meshgrid(logspace(log_K_range(1, 1), log_K_range(1,2), N_range), ...
%         logspace(log_K_range(2, 1), log_K_range(2,2), N_range));
% else
%     [K1, K2] = meshgrid(linspace(log_K_range(1, 1), log_K_range(1,2), N_range), ...
%         linspace(log_K_range(2, 1), log_K_range(2,2), N_range));
% end
% f = @(K1, K2) rfm.steady_state_output(w, N_r, [K1; K2], 1, log_K);
% 
% f_K2max = @(K1) -f(K1, K_range(2, 2));
% [K1_opt, y_K1_opt] = fminbnd(f_K2max, K_range(1, 1), K_range(1, 2));
% y_K1_opt = - y_K1_opt;
