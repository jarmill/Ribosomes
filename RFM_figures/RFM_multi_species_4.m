%general setup
N_species = 3;
rfm = RFM_Multi_Network(N_species);
rfm0 = cell(N_species, 1);

for i = 1:N_species
    rfm0{i} = RFM_Network();
end

%N_r = 3.25;
%N_r = 6;
%N_r = 35;
%N_r = 15;
N_r = 20;
%N_r =  [10, 20, 0, 10];
%N_r = [12, 3, 5];
%t_max = 120;
%t_max = 300;
%t_max = 30;
t_max = 60;
%t_max = 20;
%t_max = 30;
N_codon_max = 8;
N_strand_max = 6;


%% add rna to system
%rfm = rfm.pool_rates(0.3, 0.3, 0.3, 0.3);
%rfm = rfm.pool_rates(0.5, 0.5, 0.5, 0.5);
%tag_rate = [1, 3, 2];
%untag_rate = [0.5, 0.5, 0.5];

tag_rate   = 0.1*ones(N_species, 1);
untag_rate = 0.1*ones(N_species, 1);

%tag_rate = [1, 1,1];
%untag_rate = [1,1,1];


rfm = rfm.pool_rates(tag_rate, untag_rate);
%rfm = rfm.pool_rates(1, 1, 0.5, 0.5);

rng('default')
%rng(2)
rng(29)
mult = 1;
rand_strands = true;
for s = 1:N_species
    if rand_strands 
        num_strand = randi(N_strand_max);
    else
        num_strand = 1;
    end
    %num_strand = 1;
    for i = 1:num_strand
        N_codon = randi(N_codon_max-1)+1;
        %N_codon = 4;
        %lambda = 3*abs(randn(N_codon+1, 1));
        %mult = 1;
        if s==i==1
            %mult = 5;
            mult = 1;
        else
            mult = 1;
        end
        lambda = random('gamma', 2, 1, [N_codon+1, 1])+0.5;
        %rfm = rfm.add_rna(N_codon, lambda, s, mult);
        rfm = rfm.add_rna(mult, lambda, s);
        rfm0{s} = rfm0{s}.add_rna(mult, N_codon,lambda);
    end
end  

%on the circuit
%add an inhibitor


%get the party started

%x_end = rfm.traj.x(end, :)';
%N_leak = sum(x_end)-N_r;


%try the visualizer
DRAW =0;
OPT = 0;
OPT_LANDSCAPE = 0;

animate = 0;
    
x_ss = rfm.find_steady_state(sum(N_r));
v = RFM_Multi_Visualizer(rfm);
v.draw(Inf, x_ss);