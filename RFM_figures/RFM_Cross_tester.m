rng(20, 'twister');
r = RFM_Cross_Network( 0);
%r = r.pool_rates(0, 0);
%r = r.pool_rates(0.25, 0.25);
%r = r.pool_rates(0.15, 0.15);

%r = r.pool_rates(0.5, 0.5);

N1 = 4;
lambda1 = [2 2 2 2];
entry_lambda1 = [3 1];

N2 = 3;
lambda2 = [5 5 5];
entry_lambda2 = [1 3];

N3 = 5;
lambda3 = [3 3 3 3 3];
entry_lambda3 = [5 5];

N4 = 2;
lambda4 = [8 8] ;
entry_lambda4 = [2 4];

r = r.add_rna(N1, lambda1, entry_lambda1);
r = r.add_rna(N2, lambda2, entry_lambda2);
r = r.add_rna(N3, lambda3, entry_lambda3);
r = r.add_rna(N4, lambda4, entry_lambda4);

%x0 = [zeros(8, 1)];
%x0 = rand(8, 1);
%x0 = x0 / sum(x0) * 2;
%z0 = [1, 0.5];
%[inflow, outflow, xdot] = r.rna_flow_cross(z0, x0, r.RFM{1});

%z0 = [2; 1];
%x0 = rand(2*(N1 + N2));
%x0 = x0/sum(x0) * 8;
%xdot = r.dynamics_cross(0, [z0; x0], r.RFM);
%xdot = r.dynamics_cross_connected(0, [z0; x0], r.Pool, r.RFM);

N_r = [0 10];
t_max = 30;
r = r.simulate(N_r, t_max);

%figure(1)
%clf
%plot(r.traj.t, r.traj.x)

%try the visualizer
v = RFM_Cross_Visualizer(r);

xss = r.traj.x(end, :);
v.draw(Inf, xss)

%v.playback();
%v.playback('rna_cross_2');
% 
% ind_choose = ceil(length(r.traj.t)/2);
% %ind_choose = 100;
% x_end = r.traj.x(ind_choose , :);
% v.draw(r.traj.t(ind_choose), x_end')