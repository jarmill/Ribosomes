function xdot_stoch = RFM_Stoch(x, N_r, RFM)
    xdot = RFM_Standard(0, x, RFM);
    stoch = N_r - sum(x);
    
    xdot_stoch = [xdot; stoch];
end