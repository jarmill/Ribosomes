classdef RFM_Network
    %RFM_NETWORK Models a ribosomal flow network with a pool.
    %   Solves the system
    
    properties
        RFM;            %strands of rna in the network
        N_states;       %number of states (pools + codons)
        N_strands;
        traj;           %trajectory of solutions and information
        
        %information about the translation rates
        entry_codons;   %index of first codon on a ribosome
        exit_codons;    %index of last  codon on a ribosome
        exit_lambda;    %translation rate on a ribosome
        length_codon;   %number of codons on each rna
       
        mult;
        
    end
    
    methods
        function obj = RFM_Network()
            %constructor
            obj.RFM = {};   
            obj.N_states = 1; %pool state
            obj.N_strands = 0;
            %trajectory information
            obj.entry_codons = [];
            obj.exit_codons = [];
            obj.exit_lambda = [];
            obj.length_codon = [];
            obj.mult = [];
        end
        
        function obj = add_rna(obj, mult, N, lambda, sat_func)
            %adds a strand of RNA to the network
            
            %input:
            %   N:          number of codons on the strand of rna
            %   lambda:     initiation/elongation/translation rates
            %   sat_func:   saturation function G(z)
            
            if nargin < 5
                sat_func = @(x) tanh(x);
            end
            
            rna = struct;
            rna.N = N;
            rna.lambda = lambda;
            rna.G = sat_func;
            
            obj.RFM{end+1} = rna;
            obj.N_strands = obj.N_strands + 1;
            obj.entry_codons(end+1) = obj.N_states+1;
            obj.N_states = obj.N_states + N;
            obj.exit_lambda(end+1) = lambda(end);
            obj.exit_codons(end+1) = obj.N_states;
            obj.length_codon(end+1) = N;
            obj.mult(end+1) = mult;
            
        end
        
        function [inflow, outflow, xdot] = rna_flow(obj, z, x, rna)
            %RNA_FLOW: finds the change in ribosome content in a single strand
            %
            %Input:
            %   z:          Number of ribosomes in pool
            %   x:          Number of ribosomes in each codon
            %   rna:     	current strand of rna
            %
            %Output:
            %   inflow:     Number of ribosomes flowing in to rna (from pool)
            %   outflow:    Number of ribosomes exiting rna (translation rate)
            %   xdot:       Change in ribosomes in all codons for rna 

            %r_in = 1;
            %r_in = tanh(z);
            r_in = rna.G(z);

            x_stagger = [r_in; x; 0];
            x_transfer = rna.lambda .* x_stagger(1:end-1) .* (1-x_stagger(2:end));

            x_inflow  = x_transfer(1:end-1);
            x_outflow = x_transfer(2:end);

            %return output
            inflow = x_transfer(1);
            outflow = x_transfer(end);
            xdot = x_inflow - x_outflow;               
        end
        
        function xdot = dynamics_standard(obj, t, x, RFM)
            %function to perform derivatives
            %xdot = (1:length(x))';

            %prepare for first iteration
            z = x(1);
            ind_curr = 2;
            xdot = [];
            zdot = 0;
            for i = 1:length(RFM)
                rna_curr = RFM{i};        
                ind_next = ind_curr + rna_curr.N - 1;
                x_curr = x(ind_curr:ind_next);

                [inflow, outflow, x_curr_dot] = obj.rna_flow(z, x_curr, rna_curr);

                %prepare for next iteration
                zdot = zdot + obj.mult(i)*(outflow - inflow);
                ind_curr = ind_next + 1;   
                xdot = [xdot; x_curr_dot];
            end

            xdot = [zdot; xdot];

        end
        
        function xdot = objective_standard(obj, x, N_r, RFM)
            %objective function for RFM_Steady_State
            zxdot = obj.dynamics_standard(0, [N_r - sum(x); x], RFM);
            xdot = zxdot(2:end);
        end
        
        function [e_ss, R] = rna_steady_state(obj, RFM, gz)
            %steady state of single RFM given pool input G(z)
            %Input:
            %   RFM:    Strand of RNA
            %   gz:     G(z), saturation function applied to the pool
            %
            %Output:
            %   e_ss:   Steady state occupancy of codons x
            %   R:      Translation rate of protein at steady state
            
            if nargin < 3
                gz = 1;
            end
            
            
            lam = RFM.lambda;
            lam(1) = lam(1) * gz; %multiply initiation rate by pool saturation function
            
            [m, i] = min(lam);
            
            %if gz == 0
            if m == 0
                %pool empty, nothing coming in
                %practically never happens, but this is just in case
                R = 0;
                e_ss = zeros(size(lam(2:end)));
                
                %bottleneck in the rfm. Will need to fix this and take into
                %account N_r. 
                if i > 1
                    e_ss(1:(i-1)) = 1;
                end
                
            else
                %pool has ribosomes
                
                %form the jacobi matrix
                lam_rsq = 1./sqrt(lam);

                J = diag(lam_rsq, 1) + diag(lam_rsq, -1);

                %extract the perron eigenvector/eigenvalue of J

                %symmetric matrix means all eigenvalues are real
                [zeta, sigma] = eigs(J, 1, 'largestreal');

                %optimal flow rate of output            
                %from spectral representation
                R = 1/(sigma^2);

                %e_ss = lam_rsq/sigma .* zeta(2:end)./zeta(1:end-1);
                e_ss = lam_rsq(2:end)/sigma .* zeta(3:end)./zeta(2:end-1);
            end
            
        end
        
        function [N_r, x_ss] = network_steady_state(obj, z, RFM)
            %objective function, determines the total ribosomal occupancy
            %in the network                                    
            N_r = z;
            x_ss = [z];
            for i = 1:length(RFM)
                rna_curr = RFM{i};        
                gz = rna_curr.G(z);
                
                e_ss_curr = obj.rna_steady_state(rna_curr, gz);
                N_curr = sum(e_ss_curr);
                
                x_ss = [x_ss; e_ss_curr];
                
                N_r = N_r + obj.mult(i)*N_curr;

            end
        end
        
        function x_ss = find_steady_state(obj, N_r)
            %use bisection to find optimal steady state
            err_min = 1e-6;
            err = Inf;
            a = 0;
            b = N_r;
            
            
            while abs(err) > err_min
                z_curr = (a+b)/2;
               
                [N_curr, x_ss] = network_steady_state(obj, z_curr, obj.RFM);
                
                err = N_curr - N_r;
                if err > 0
                    b = z_curr;
                else
                    a = z_curr;
                end
            end
            
        end
        
        function y = steady_state_output(obj, w, N_r)
                x_ss = obj.find_steady_state(N_r);
                y_ss = obj.output(x_ss);
                y  = w'*y_ss;
        end
        
        function  y = output(obj, x)
            %exit_ind =  obj.exit_codons;
            %mult = obj.mult{:};
            %exit_lambda = cat(2, obj.exit_lambda{:});
            y = (obj.mult.*obj.exit_lambda)' .* x(obj.exit_codons);
        end
        
        function x_ss = steady_state(obj, N_r)
            %Compute the steady state number of ribosomes in each codon and the
            %pool. Take advantage of the stochiometric constraint.

            %if nargin < 4
                options = optimoptions('fsolve', 'Display', 'off');
            %end

            %find number of codons    
            N = obj.N_states;

            %anonymous functions
            f = @(x) obj.objective_standard(x, N_r, obj.RFM);

            x0 = zeros(N-1, 1);

            x_ss_x = fsolve(f, x0, options);
            x_ss = [N_r - sum(x_ss_x); x_ss_x];
        end
        
        function obj = simulate(obj, N_r, t_max)
            %solve the differential equation in dynamics_standard
            
            traj_out = struct;
            traj_out.N_r = N_r;
            
            t_range = [0 t_max];
            x0 = zeros(obj.N_states, 1);
            x0(1) = N_r;
            
            %find the trajectory of motion
            [T, X] = ode45(@obj.dynamics_standard, t_range, x0, [], obj.RFM);
            
            traj_out.t = T;
            traj_out.x = X;                        
            traj_out.y = X(:, obj.exit_codons) * diag(obj.exit_lambda);
            
            %steady state solutions
            %x_ss = obj.steady_state(N_r);
            x_ss = obj.find_steady_state(N_r);
            y_ss = obj.exit_lambda' .* x_ss(obj.exit_codons);
            
            traj_out.x_ss = x_ss;
            traj_out.y_ss = y_ss;
            
            traj_out.spline = spline(T, X');
            
            obj.traj = traj_out;
            
        end       

    end
end

