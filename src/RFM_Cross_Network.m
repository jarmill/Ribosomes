classdef RFM_Cross_Network
    %RFM_Split_NETWORK Models a ribosomal flow network with a pool.
    %ribosomes are split into empty, host, and circuit ribosomes
    %(orthogonal). 
    %   Solves the system
    
    properties
        %structs
        Pool;                %properties of the pool (transition rates for tagging)
        RFM;                 %strands of rna in the network (on circuit)
        Inhibitor;           %protein that inhibits expression of the orthogonal ribosome
        N_states;       %number of states (pools + codons)
        traj;           %trajectory of solutions and information
        simple_model;       %reduced (two-tank) model or full (three-tank) model
        %information about the translation rates
        entry_codons_host;   %index of first codon on RNA (host)
        exit_codons_host;    %index of last codon on RNA  (host)
        entry_codons_circ;   %index of first codon on a RNA (circ)
        exit_codons_circ;    %index of last  codon on a RNA (circ)
        
        entry_lambda;        %initiation rate for each pool
        exit_lambda;         %translation rate on a ribosome
        length_codons;        %number of codons on each rna

    end
    
    methods
        function obj = RFM_Cross_Network(simple_model)
            %constructor
            if nargin < 1
                obj.simple_model = 0;
            else
                obj.simple_model = simple_model;
            end
            
            obj.RFM = {};   
            
            
            obj.Pool = struct;            
            
            obj.Pool.host_tag_rate   = 0.1;
            obj.Pool.circ_tag_rate   = 0.1;
            
            if obj.simple_model
                obj.N_states = 2;
                obj.Pool.num_pool = 2;
            else
                obj.N_states = 3; %pool states included by default (empty, host, circuit)
                obj.Pool.host_untag_rate = 0.1;
                obj.Pool.circ_untag_rate = 0.1; 
                obj.Pool.num_pool = 3;
            end
            
            %TODO: implement inhibitor for crosstalk system (or not)
            obj.Inhibitor = [];
            
            %trajectory information
            obj.entry_codons_host = [];
            obj.exit_codons_host = [];
            obj.entry_codons_circ = [];
            obj.exit_codons_circ = [];
            
            obj.entry_lambda = [];
            obj.exit_lambda = [];
            obj.length_codons = [];
        end
        
        function obj = pool_rates(obj, host_tag_rate, circ_tag_rate, host_untag_rate, circ_untag_rate)
            %set the rates of tagging in the pool
            if obj.simple_model
                obj.Pool.host_tag_rate   = host_tag_rate;
                obj.Pool.circ_tag_rate   = circ_tag_rate;
            else    
                obj.Pool.host_tag_rate   = host_tag_rate;
                obj.Pool.circ_tag_rate   = circ_tag_rate;
                obj.Pool.host_untag_rate = host_untag_rate;
                obj.Pool.circ_untag_rate = circ_untag_rate;
            end
        end
        
%         function obj = add_inhibitor(obj, p0, kf, delta)
%             %properties of the inhibitor
%             %Input:
%             %   p0:     reference number of protein
%             %   kf:     Hill coefficient
%             %   delta:  degradatioon rate
%             
%             obj.Inhibitor = struct;
%             obj.Inhibitor.p0 = p0; 
%             obj.Inhibitor.kf = kf;
%             obj.Inhibitor.delta = delta;
%             
%             %add a new state for the protein inhibitor
%             obj.N_states = obj.N_states+1;
%             
%             %the first dna strand on the circuit is the controller,
%             %produces the protein inhibitor
%         end
        
        function obj = add_rna(obj, N, lambda, entry_lambda, sat_func)
            %adds a strand of RNA to the network
            %Input:
            %   N:      number of codons on the rna strand
            %   lambda: transition rates of rna (translation, elongation,
            %           initiation)
            %   entry_lambda: initiation rates from each pool [lam_host,
            %                 lam_circ]. If lam_circ = 0, then this rna is
            %                 purely from the host pool. Vice versa if
            %                 lam_host = 0. If lam_host > 0 and lam_circ >
            %                 0, then there is crosstalk on the network,
            %                 and both types of ribosomes are found on the
            %                 RNA.
            
            %From which pool do the ribosomes come?
            if nargin < 4
                entry_lambda = [lambda(1) 0];                
            end
                        
            lam_host = entry_lambda(1);
            lam_circ = entry_lambda(2);
            
            %saturation function on the pool, assume tanh for now
            %in reality, each pool has its own saturation function. Add
            %later.
            if nargin < 5
                sat_func = @(x) tanh(x);
            end
            
            rna = struct;
            rna.N = 2*N; %each codon can take ribosomes from either pool, so there are twice as many codons
            rna.lambda = lambda;
            rna.entry_lambda = entry_lambda;
            rna.G = sat_func;
            
            obj.RFM{end+1} = rna;
                        
            %update the trackers
            obj.entry_lambda(:, end+1) = entry_lambda;
            obj.exit_lambda(end+1) = lambda(end);            
            obj.length_codons(end+1) = 2*N;
                        
            obj.entry_codons_host(end+1) = 1 + obj.N_states;
            obj.entry_codons_circ(end+1) = 2 + obj.N_states;
            obj.exit_codons_host(end+1) = 2*N -1 + obj.N_states;
            obj.exit_codons_circ(end+1) = 2*N + obj.N_states;
            
%             if source == 0
%                 %on the host
%                 
%                 %add to the rfm
%                 obj.RFM_host{end+1} = rna;
%                 
%                 %update trackers for host
%                 
%                 obj.exit_lambda_host(end+1) = lambda(end);
%                 obj.entry_codons_host(end+1) = num_pools  + sum(obj.length_codons_host) + 1;
%                 obj.exit_codons_host(end+1) = num_pools + sum(obj.length_codons_host) + N;
%                 obj.length_codons_host(end+1) = N;
%                 
%                 %update trackers for circuit
%                 obj.entry_codons_circ = obj.entry_codons_circ + N;
%                 obj.exit_codons_circ = obj.entry_codons_circ + N;                
%             else
%                 %on the circuit
%                 
%                 %add to the rfm
%                 obj.RFM_circ{end+1} = rna;
%                 
%                 %update trackers for circuit
%                 obj.exit_lambda_circ(end+1) = lambda(end);
%                 obj.entry_codons_circ(end+1) = num_pools + sum(obj.length_codons_host) + sum(obj.length_codons_circ) + 1;
%                 obj.exit_codons_circ(end+1) = num_pools + sum(obj.length_codons_host) + sum(obj.length_codons_circ) + N;
%                 obj.length_codons_circ(end+1) = N;                                
%             end
%             
             obj.N_states = obj.N_states + 2*N;            
        end
        
        function [inflow, outflow, xdot] = rna_flow_cross(obj, z, x, rna)
            %RNA_FLOW_CROSS: finds the change in ribosome content in a
            %single strand in a crosstalk model
            %
            %Input:
            %   z:          Number of ribosomes in pool
            %   x:          Number of ribosomes in each codon
            %   rna:     	current strand of rna
            %
            %Output:
            %   inflow:     Number of ribosomes flowing in to rna (from pools)
            %   outflow:    Number of ribosomes exiting rna (translation rate)
            %   xdot:       Change in ribosomes in all codons for rna 

            %r_in = 1;
            %r_in = [rna.G(z(1)); rna.G(z(2))];
            
            %incoming ribosomes
            r_in_host = rna.G(z(1));
            r_in_circ = rna.G(z(2));
            
            x_host = x(1:2:end);
            x_circ = x(2:2:end);
            
            lambda_host = [rna.entry_lambda(1); rna.lambda'];
            lambda_circ = [rna.entry_lambda(2); rna.lambda'];
            
            x_total = x_host + x_circ;
            
            %Find  the rates of travel between components
            x_host_aug = [r_in_host; x_host];
            x_circ_aug = [r_in_circ; x_circ];
            x_total_aug = [x_total; 0];
            
            x_transfer_host = lambda_host .* x_host_aug .* (1 - x_total_aug);
            x_transfer_circ = lambda_circ .* x_circ_aug .* (1 - x_total_aug);
            
            xdot_host = -diff(x_transfer_host);
            xdot_circ = -diff(x_transfer_circ);                        
            
            %rewrite this to generalize later to multiple pools (more than two)               
            
            inflow  = [x_transfer_host(1); x_transfer_circ(1)];
            outflow = [x_transfer_host(end); x_transfer_circ(end)];
            xdot = zeros(size(x));
            xdot(1:2:end) = xdot_host;
            xdot(2:2:end) = xdot_circ;
        end
        
        function xdot = dynamics_cross(obj, t, x, RFM)
            %function to perform derivatives on crosstalk model
            %xdot = (1:length(x))';

            %prepare for first iteration
            z = x(1:2);
            ind_curr = 3;
            xdot = [];
            zdot = [0;  0];
            for i = 1:length(RFM)
                rna_curr = RFM{i};        
                ind_next = ind_curr + rna_curr.N - 1;
                x_curr = x(ind_curr:ind_next);

                [inflow, outflow, x_curr_dot] = obj.rna_flow_cross(z, x_curr, rna_curr);

                %prepare for next iteration
                zdot = zdot + outflow - inflow;
                %xdot(-2 + (ind_curr:ind_next)) =  x_curr_dot;
                xdot = [xdot; x_curr_dot];
                
                ind_curr = ind_next + 1;   
                
            end

            xdot = [zdot; xdot];

        end
        
        
        function xdot = dynamics_cross_connected(obj, t, x, Pool, RFM, Inhibitor)
            %function to perform derivatives over crosstalk ribosome network
            
            %extract properties from x
            if obj.simple_model
                z_host = x(1);
                z_circ = x(2);    
                x_network = x;
            else               
                z_empty = x(1);
                z_host = x(2);
                z_circ = x(3);   
                x_network = x(2:end);
            end
            
            
            xdot = obj.dynamics_cross(t, x_network, RFM);
            zdot_host = xdot(1);
            zdot_circ = xdot(2);
            x_block = xdot(3:end);
            
            %x_network = x(obj.Pool.num_pools+1:end);
            %z = [z_host; z_circ];
            
%             %inhibitor
%             if isempty(Inhibitor)
%                 p_f = 0;
%                 p0 = 1;
%                 kf = 1;
%             else
%                 p_f = x(end); %inhibitor proteins
%                 p0 = Inhibitor.p0;
%                 kf = Inhibitor.kf;
%                 delta = Inhibitor.delta;
%             end
%             
            %find the host and circuit derivatives
            %xdot_network_host = obj.dynamics_standard(t, network_host, RFM_host);
            %xdot_network_circ = obj.dynamics_standard(t, network_circ, RFM_circ);
            
            %extract properties from these xdots
            %zdot_host = xdot_network_host(1);
            %xdot_host = xdot_network_host(2:end);
            %zdot_circ = xdot_network_circ(1);
            %xdot_circ = xdot_network_circ(2:end);
            
            %mass action to find rate changes among tagging
            %hill_inhibition = (1/(1 + (p_f/p0))^kf);
            %hill_inhibition = 1/((p0/p_f)^kf + 1);
            hill_inhibition = 1;
            
            
            if obj.simple_model
                zdot_host = zdot_host + z_circ * Pool.host_tag_rate - z_host * Pool.circ_tag_rate * hill_inhibition;
                zdot_circ = zdot_circ + z_host * Pool.circ_tag_rate * hill_inhibition - z_circ * Pool.host_tag_rate;
                z_block = [zdot_host; zdot_circ];
            else
                zdot_empty = z_host * Pool.host_untag_rate + z_circ * Pool.circ_untag_rate ...
                         - z_empty * (Pool.host_tag_rate + Pool.circ_tag_rate * hill_inhibition);
                zdot_host = zdot_host + z_empty * Pool.host_tag_rate - z_host * Pool.host_untag_rate;
                zdot_circ = zdot_circ + z_empty * Pool.circ_tag_rate * hill_inhibition - z_circ * Pool.circ_untag_rate;
                z_block = [zdot_empty; zdot_host; zdot_circ];            
            end
            
            %output is concatenation of all the stuff
%             if isempty(Inhibitor)
%                 p_f_dot = [];
%             else
%                 p_f_in = x(obj.exit_codons_circ(1)) * obj.exit_lambda_circ(1);
%                 p_f_out = delta*p_f;
%                 p_f_dot = p_f_in - p_f_out;
%             end
%             xdot = [z_block; xdot_host; xdot_circ; p_f_dot];
            xdot = [z_block; x_block];
        end
        
        
       function [e_ss, R] = rna_steady_state(~, RFM, gz)
            %steady state of single RFM given pool input G(z)
            %Input:
            %   RFM:    Strand of RNA
            %   gz:     G(z), saturation function applied to the pool
            %
            %Output:
            %   e_ss:   Steady state occupancy of codons x
            %   R:      Translation rate of protein at steady state
            
            if nargin < 3
                gz = 1;
            end
            
            
            lam = RFM.lambda;
            lam(1) = lam(1) * gz; %multiply initiation rate by pool saturation function
            
            [m, i] = min(lam);
            
            %if gz == 0
            if m == 0
                %pool empty, nothing coming in
                %practically never happens, but this is just in case
                R = 0;
                e_ss = zeros(size(lam(2:end)));
                
                %bottleneck in the rfm. Will need to fix this and take into
                %account N_r. 
                if i > 1
                    e_ss(1:(i-1)) = 1;
                end
                
            else
                %pool has ribosomes
                
                %form the jacobi matrix
                lam_rsq = 1./sqrt(lam);

                J = diag(lam_rsq, 1) + diag(lam_rsq, -1);

                %extract the perron eigenvector/eigenvalue of J

                %symmetric matrix means all eigenvalues are real
                [zeta, sigma] = eigs(J, 1, 'largestreal');

                %optimal flow rate of output            
                %from spectral representation
                R = 1/(sigma^2);

                %e_ss = lam_rsq/sigma .* zeta(2:end)./zeta(1:end-1);
                e_ss = lam_rsq(2:end)/sigma .* zeta(3:end)./zeta(2:end-1);
            end
            
        end

        
        function [N_r, x_ss] = network_steady_state(obj, z, r)
            %objective function, determines the total ribosomal occupancy
            %in the network
                                    
            
            %find the pool splitting steady state
            if obj.simple_model
                if nargin < 3
                    split_mat = [obj.Pool.circ_tag_rate -obj.Pool.host_tag_rate;
                                1 1];
                    z_split = split_mat \ [0; z];
                else
                    split_mat = [1 -r;
                                 1 1];
                             
                    if r == Inf
                        z_split = [z; z];
                    else
                        z_split = split_mat \ [0; z];
                    end
                end
                

            else
                split_mat = [obj.Pool.host_tag_rate  0 -obj.Pool.circ_untag_rate;
                             0 obj.Pool.circ_tag_rate -obj.Pool.circ_untag_rate;
                             1 1 1];
                z_split = split_mat \ [0; 0; z];  
            end
            N_r = z;
            x_ss = [z_split];                                  
            
            for i = 1:length(obj.RFM_host)
                rna_curr = obj.RFM_host{i};        
                gz = rna_curr.G(z_split(1));
                
                e_ss_curr = obj.rna_steady_state(rna_curr, gz);
                N_curr = sum(e_ss_curr);
                
                x_ss = [x_ss; e_ss_curr];
                
                N_r = N_r + N_curr;
            end
            
            for i = 1:length(obj.RFM_circ)
                rna_curr = obj.RFM_circ{i};        
                gz = rna_curr.G(z_split(2));
                
                e_ss_curr = obj.rna_steady_state(rna_curr, gz);
                N_curr = sum(e_ss_curr);
                
                x_ss = [x_ss; e_ss_curr];
                
                N_r = N_r + N_curr;
            end
            
        end
        
        function y_out = get_translation_rate(obj, x_in)
            %find translation rates at a particular x
            
            y_host = obj.exit_lambda_host' .* x_in(obj.exit_codons_host);
            y_circ = obj.exit_lambda_circ' .* x_in(obj.exit_codons_circ);
            
            y_out = [y_host; y_circ];
            
        end
        
        function x_ss = find_steady_state(obj, N_r, r)
            %use bisection to find optimal steady state
            err_min = 1e-6;
            err = Inf;
            a = 0;
            b = N_r;                                                            
            
            while abs(err) > err_min
                z_curr = (a+b)/2;
                
                %[zh; zc; ze];

                if nargin < 3
                    [N_curr, x_ss] = network_steady_state(obj, z_curr);
                else
                    [N_curr, x_ss] = network_steady_state(obj, z_curr, r);
                end
                
                err = N_curr - N_r;
                if err > 0
                    b = z_curr;
                else
                    a = z_curr;
                end
            end
            
        end
        
        function x_ss = steady_state(obj, N_r)
            %Compute the steady state number of ribosomes in each codon and the
            %pool. Take advantage of the stochiometric constraint.

            %if nargin < 4
                options = optimoptions('fsolve', 'Display', 'off');
            %end

            %find number of codons    
            N = obj.N_states;

            %anonymous functions
            f = @(x) obj.objective_split(x, N_r, obj.Pool, obj.RFM_host, obj.RFM_circ);

            x0 = zeros(N-1, 1);

            x_ss_x = fsolve(f, x0, options);
            x_ss = [N_r - sum(x_ss_x); x_ss_x];
        end
        
        function obj = simulate(obj, N_r, t_max)
            %solve the differential equation in dynamics_standard
            
            traj_out = struct;
            traj_out.N_r = sum(N_r);
            
            t_range = [0 t_max];
            x0 = zeros(obj.N_states, 1);
            if length(N_r) == 2
                %put ribosomes in the host/circuit pools
                if obj.simple_model
                    x0(1) = N_r(1);
                    x0(2) = N_r(2);
                else
                    x0(2) = N_r(1);
                    x0(3) = N_r(2);
                end
            else
                %put ribosomes in the empty pool
                x0(1) = N_r;
            end
            
            %find the trajectory of motion
            [T, X] = ode45(@obj.dynamics_cross_connected, t_range, x0, [], obj.Pool, obj.RFM, obj.Inhibitor);
            
            traj_out.t = T;
            traj_out.x = X;  
            traj_out.y_host = X(:, obj.exit_codons_host) * diag(obj.exit_lambda);            
            traj_out.y_circ = X(:, obj.exit_codons_circ) * diag(obj.exit_lambda);
            traj_out.y = traj_out.y_host + traj_out.y_circ;
            
            %traj_out.y = X(:, obj.exit_codons) * diag(obj.exit_lambda);
            
%             %steady state solutions
%             x_ss = obj.steady_state(sum(N_r));
%             y_host_ss = obj.exit_lambda_host' .* x_ss(obj.exit_codons_host);
%             y_circ_ss = obj.exit_lambda_circ' .* x_ss(obj.exit_codons_circ);
% 
%             traj_out.x_ss = x_ss;
%             traj_out.y_host_ss = y_host_ss;            
%             traj_out.y_circ_ss = y_circ_ss;
             %coefficients of cubic spline in order to find X trajectory (I think)
             traj_out.spline = spline(T, X');
            
            obj.traj = traj_out;
            
        end       

    end
end

