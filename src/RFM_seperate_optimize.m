function [f_opt, N_opt] = RFM_seperate_optimize(RFM, w, N_r)
    %No empty pool, no interaction between different species
    %optimize the weighted output with respect to equilibrium
    %number of ribosomes in the pools
    
    
    N_species = length(RFM);
    
    %box constraints
    lb = zeros(N_species-1, 1);
    ub = N_r * ones(N_species-1, 1);

    %sum of species
    A = ones(1, N_species-1);
    b = N_r;
    
    f = @(N) -RFM_separate_objective(RFM, w, [N; N_r-sum(N)]);

    x0 = ones(N_species-1, 1)/N_species * N_r;

    [N_part_opt, f_opt] = fmincon(f,x0,A,b,[], [], lb,ub);
    f_opt = -f_opt;

    N_opt = [N_part_opt; N_r- sum(N_part_opt)];            
end

