classdef RFM_Single_Cross_Visualizer
    %UNTITLED3 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        %trajectory parameters
        sample_rate;
        lambda;
        plant;
        
        
        %visualization parameters
        w_codon = 0.2;
        h_codon = 0.2;
        offset_codon = 0.4; 
        outside_angle = pi/6;
        outside_radius = 0.75;
        
        axis_limit;
        
        %width_mult = 6;
        width_mult = 20;
        
        colors;
    end
    
    methods
        function obj = RFM_Single_Cross_Visualizer(plant, sample_rate)
            %UNTITLED3 Construct an instance of this class
            %   Detailed explanation goes here
            if nargin < 2
                obj.sample_rate = 0.05;
            else
                obj.sample_rate = sample_rate;
            end
            
            obj.plant = plant;
        end
        
        function draw(obj, t, x)
            %Draw the Ribosomal Flow Network
            
            persistent hFig codon;
            if (isempty(hFig) || ~isvalid(hFig))
                hFig = figure(26);
                set(hFig,'DoubleBuffer', 'on');
                
                codon = [obj.w_codon/2*[1 -1 -1 1]; obj.h_codon/2*[1 1 -1 -1]];
                %pool = [obj.w_pool/2*[1 -1 -1 1]; obj.h_pool/2*[1 1 -1 -1]];
                %th = linspace(0, 2*pi, 150);
                %circ = [cos(th); sin(th)];
            end
            
            figure(hFig); cla; hold on; view(0,90);
%             obj.colors = get(gca,'colororder');
            %obj.colors = eye(3);
            obj.colors = linspecer(obj.plant.N_species);
%             obj.colors = [1; 1; 1]'*0.5;
            %extract data
            X = reshape(x, [obj.plant.N_species, obj.plant.N_codons]);
            [flows, xdot] = obj.plant.rna_flow(x);
            %cX = cumsum([zeros(1, obj.plant.N_codons); X], 1);
            
            %start drawing
            
            %flows            
            %
            if obj.plant.N_species == 1;
                offset_theta = 0;
            else
                offset_theta = linspace(-obj.outside_angle, obj.outside_angle, obj.plant.N_species);
            end
            
            for j = 1:obj.plant.N_species
                
                %between codons
                for i = 1:(obj.plant.N_codons-1)
                    offset_row = obj.offset_codon*(i-1);
                    if flows(j, i+1) > 0
                        line(obj.offset_codon*0.5 + offset_row +obj.w_codon/2*[-1, 1], ...
                            [0, 0], 'LineWidth', obj.width_mult*flows(j, i+1),...
                            'Color', [obj.colors(j, :), 0.5]);
                    end
                end            
                
                %source and sink
                theta = offset_theta(j);
                %source
                if flows(j, 1) > 0
                    line(0+ [0, obj.outside_radius*cos(pi-theta)], [0, 0 + obj.outside_radius*sin(pi-theta)],...
                         'LineWidth', obj.width_mult*flows(j, 1),...
                                'Color', [obj.colors(j, :), 0.5])
                end
                
                %sink
                if flows(j, end) > 0
                    line( obj.offset_codon*(obj.plant.N_codons-1)+ [0, obj.outside_radius*cos(theta)], [0, 0 + obj.outside_radius*sin(theta)],...
                         'LineWidth', obj.width_mult*flows(j, end),...
                                'Color', [obj.colors(j, :), 0.5])
                end
            end
            
            
            %codons
            for i = 1:obj.plant.N_codons
                x_curr = X(:, i);
                cX  = cumsum([0; x_curr]);
                offset_row = obj.offset_codon*(i-1);
               
                %outside of codon
                for j = 1:obj.plant.N_species
                    fill_x = codon(1, :) + offset_row;
                    %fill_y = codon(2, :);
                    %fill_y = codon(2, :) + obj.h_codon*cX(j) + x_curr(j)*codon(2, :) - obj.h_codon/2*(1-x_curr(j));
                    fill_y = obj.h_codon*cX(j) + x_curr(j)*codon(2, :) - obj.h_codon/2*(1-x_curr(j));
                    patch(fill_x, fill_y, obj.colors(j, :), 'EdgeColor', 'None');
                end
                
                %fill in blank and wrap boundary
                fill_x = codon(1, :) + offset_row;
                fill_y = obj.h_codon*cX(j+1) + (1 -cX(j+1))*codon(2, :) - obj.h_codon/2*(cX(j+1));
                patch(fill_x, fill_y, 'White', 'EdgeColor', 'None');
                
                patch(codon(1, :) +  offset_row, codon(2, :), 'k', 'FaceColor', 'None', 'LineWidth', 2)
                    
            end
            
            %axis and featurs
            axis image;
            title(['t = ', num2str(t(1),'%.2f') ' sec']);
            set(gca,'XTick',[],'YTick',[])
            set(gca,'XColor','none','YColor','none')

        end
        
        function M = playback(obj, filename)
            %run the animation
            
            if (nargin < 2)
                record = 0;
            else
                record = 1;
            end
            
            t_min = obj.plant.traj.spline.breaks(1);
            t_max = obj.plant.traj.spline.breaks(end);
            
            t = t_min:obj.sample_rate:t_max;            
            
            x_interp = ppval(obj.plant.traj.spline, t);
            
            %set up folder
            if record && (exist(['img/', filename], 'dir') == 0)
                mkdir(['img/', filename]);
            end
            
            for i = 1:length(t)
                t_curr = t(i);
                x_curr = x_interp(:, i);
                
                obj.draw(t_curr, x_curr);
                
                %print out the images
                if record
                    print(['img/', filename, '/Frame ' num2str(i)], '-dpng', '-r150');
                end
            end
            
            if record
                %save the output
                GifName = strcat('img/gif_out/', filename, '.gif');
                delay = obj.sample_rate;
                for i = 1:length(t)
                    [A, ~] = imread(['img/', filename, '/Frame ', num2str(i), '.png']);
                    [X, map] = rgb2ind(A, 256);
                    if i == 1
                        imwrite(X, map, GifName, 'gif', 'LoopCount', inf, 'DelayTime', delay)
                    else
                        imwrite(X, map, GifName, 'gif', 'WriteMode', 'append', 'DelayTime', delay)
                    end
                end
            end
        end
        
    %end of methods
    end
end

