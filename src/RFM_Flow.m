function [inflow, outflow, xdot] = RFM_Flow(z, x, lambda)
    %RFM_FLOW: finds the change in ribosome content in a single strand
    %
    %Input:
    %   z:          Number of ribosomes in pool
    %   x:          Number of ribosomes in each codon
    %   lambda:     Initiation/Elongation/Translation rate between codons
    %
    %Output:
    %   inflow:     Number of ribosomes flowing in to rna (from pool)
    %   outflow:    Number of ribosomes exiting rna (translation rate)
    %   xdot:       Change in ribosomes in all codons for rna 
    
    %r_in = 1;
    r_in = tanh(z);
    
    x_stagger = [r_in; x; 0];
    x_transfer = lambda .* x_stagger(1:end-1) .* (1-x_stagger(2:end));

    x_inflow  = x_transfer(1:end-1);
    x_outflow = x_transfer(2:end);

    %return output
    inflow = x_transfer(1);
    outflow = x_transfer(end);
    xdot = x_inflow - x_outflow;               
end
