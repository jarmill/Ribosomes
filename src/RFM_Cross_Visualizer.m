classdef RFM_Cross_Visualizer
% Implements the draw function for the Flow Engine model
% based on heave dynamics of an air cushioned vehicle

    properties
        %trajectory parameters
        sample_rate;
        traj;
        RFM;
        simple_model;
        Inhibitor;
        Pool;
        
        %information about the translation rates
        entry_codons_host;   %index of first codon on RNA (host)
        exit_codons_host;    %index of last codon on RNA  (host)
        entry_codons_circ;   %index of first codon on a RNA (circ)
        exit_codons_circ;    %index of last  codon on a RNA (circ)
        
        entry_lambda;        %initiation rate for each pool
        exit_lambda;         %translation rate on a ribosome
        length_codons;        %number of codons on each rna

        
        %visualization parameters
        w_codon = 0.2;
        h_codon = 0.2;
        offset_codon = 0.4;
        offset_rna = 0.2;
        axis_limit;
        
        sum_radius = 0.1;
        
        width_mult = 6;
        
        w_pool = 0.4;
        h_pool = 0.8;
        pool_shift = -0.9;
        pool_shift_vert = 1.5;
        pool_drop = 1.4;
        
        colors;
    end

    methods
        function obj = RFM_Cross_Visualizer(plant, sample_rate)
            obj.RFM = plant.RFM;
            obj.simple_model = plant.simple_model;
            obj.Inhibitor = plant.Inhibitor;
            obj.Pool = plant.Pool;
            obj.traj = plant.traj;
            obj.pool_shift_vert = 1.5*obj.h_pool;
            
            %shortcuts here
            obj.entry_codons_host = plant.entry_codons_host;            
            obj.exit_codons_host  = plant.exit_codons_host;
            obj.length_codons = plant.length_codons;
            
            obj.entry_codons_circ = plant.entry_codons_circ;            
            obj.exit_codons_circ  = plant.exit_codons_circ;
            
            obj.exit_lambda = plant.exit_lambda;

            if obj.simple_model
                obj.axis_limit = [obj.pool_shift - 0.2 (obj.offset_codon*max(obj.length_codons)/2 + 0.6) -0.8-obj.pool_shift_vert 0.8];
            else
                obj.axis_limit = [-obj.w_pool/2 + 1.599*obj.pool_shift - 0.2 (obj.offset_codon*max(obj.length_codons)/2 + 0.6) -0.8-obj.pool_shift_vert 0.8];
            end
            
            %colors
            obj.colors = struct;
            obj.colors.circ = [247,120,120]/255;
            obj.colors.host = [78,206,255]/255;
            obj.colors.empty = [216,191,216]/255;
            obj.colors.inhibitor = [238,232,170]/255;  
            if nargin < 2
                obj.sample_rate = 0.05;
            else
                obj.sample_rate = sample_rate;
            end
        end
    
        function draw(obj,t,x)
            % Draw the Ribosomal Flow Network
            %       persistent hFig skate bag;
            persistent hFig codon pool circ;
      
            if (isempty(hFig) || ~isvalid(hFig))
                hFig = figure(25);
                set(hFig,'DoubleBuffer', 'on');
                
                codon = [obj.w_codon/2*[1 -1 -1 1]; obj.h_codon/2*[1 1 -1 -1]];
                pool = [obj.w_pool/2*[1 -1 -1 1]; obj.h_pool/2*[1 1 -1 -1]];
                th = linspace(0, 2*pi, 150);
                circ = [cos(th); sin(th)];
            end
            
            figure(hFig); cla; hold on; view(0,90);

            %extract data
            if obj.simple_model
                z_host = x(1);
                z_circ = x(2);
            else
                z_empty = x(1);
                z_host  = x(2);
                z_circ  = x(3);
            end
            
%             x_host = x(obj.entry_codons_host(1):obj.exit_codons_host(end));
%             if isempty(obj.RFM_circ)
%                 x_circ = [];
%             else
%                 x_circ = x(obj.entry_codons_circ(1):obj.exit_codons_circ(end));
%             end

            pool_edge = obj.w_pool/2 - 0.5;
            
            %% draw the RNA (host)
            N_rna = length(obj.RFM);
            
            %handle the column offsets
            possible_col = linspace(1.5*obj.h_pool, -1.5*obj.h_pool, N_rna + 2) -  obj.pool_shift_vert/2;
            col_list = possible_col(2:end-1);
            
            outflow = 0;
            ind = obj.Pool.num_pool+1;
            for r = 1:N_rna
                %get the current states along the RNA
                offset_col = col_list(r);
                rna_curr = obj.RFM{r};
                
                %occupancy
                x_host = x(ind:2:(ind+rna_curr.N-1));
                x_circ = x((ind+1):2:(ind+rna_curr.N));
                x_total = x_host + x_circ;

                %transfers
                lambda_host = [rna_curr.entry_lambda(1); rna_curr.lambda'];
                lambda_circ = [rna_curr.entry_lambda(2); rna_curr.lambda'];
                
                r_in_host = rna_curr.G(z_host);
                r_in_circ = rna_curr.G(z_circ);
                
                %Find  the rates of travel between components
                x_host_aug = [r_in_host x_host];
                x_circ_aug = [r_in_circ x_circ];
                x_total_aug = [x_total 0];

                x_transfer_host = lambda_host .* x_host_aug .* (1 - x_total_aug);
                x_transfer_circ = lambda_circ .* x_circ_aug .* (1 - x_total_aug);
                %summing junction setup, centered at (summer_x, 0)
                summer_x = (obj.offset_codon*(max(obj.length_codons)/2 + 1.25)); 
                %+ 4*obj.sum_radius);
                end_terminal_row = obj.offset_codon*(max(obj.length_codons)/2);
                %Draw transfer from pool
                %if x_transfer(1) > 0
                %    line([pool_edge; -obj.w_codon/2], [offset_col; offset_col], 'LineWidth', obj.width_mult*x_transfer(1), 'Color', obj.colors.host);
                %end
                
               
               for i = 1:(rna_curr.N/2)
                    %plot position of each codon
                     offset_row = obj.offset_codon*(i-1);
               
                     %pool to codon through flow terminal
                     if i == 1
                         if x_transfer_host(1) > 0
                             line([obj.pool_shift + obj.w_pool/2 , obj.offset_codon*0.5 - obj.offset_codon - obj.w_codon/2], [0, offset_col], 'LineWidth', obj.width_mult*x_transfer_host(1), 'Color', [obj.colors.host, 0.5]);               
                             line(obj.offset_codon*0.5 - obj.offset_codon +obj.w_codon/2*[-1, 1], [offset_col; offset_col], 'LineWidth', obj.width_mult*x_transfer_host(1), 'Color', [obj.colors.host, 0.5]);
                         end
                         if x_transfer_circ(1)  > 0
                             line(obj.offset_codon*0.5 - obj.offset_codon +obj.w_codon/2*[-1, 1], [offset_col; offset_col], 'LineWidth', obj.width_mult*x_transfer_circ(1), 'Color', [obj.colors.circ, 0.5]);
                             line([obj.pool_shift + obj.w_pool/2 , obj.offset_codon*0.5 - obj.offset_codon - obj.w_codon/2], [-obj.pool_shift_vert, offset_col], 'LineWidth', obj.width_mult*x_transfer_circ(1), 'Color', [obj.colors.circ, 0.5]);
               
                         end
                     end
               
                     if (x_transfer_host(i+1) > 0) %&& (i < rna_curr.N/2)
                         if i == rna_curr.N/2
                             line([(obj.offset_codon*0.5 + offset_row +obj.w_codon/2*-1); end_terminal_row; summer_x], [offset_col; offset_col; 0], 'LineWidth', obj.width_mult*x_transfer_host(i+1), 'Color', [obj.colors.host, 0.5]);
                         else
                             line(obj.offset_codon*0.5 + offset_row +obj.w_codon/2*[-1, 1], [offset_col; offset_col], 'LineWidth', obj.width_mult*x_transfer_host(i+1), 'Color', [obj.colors.host, 0.5]);
                         end
                          %                         if i == rna_curr.N
%                             %blah
%                             %line(obj.offset_codon*0.5 + offset_row +obj.w_codon/2*[-1, 1], [offset_col; offset_col], 'LineWidth', obj.width_mult*x_transfer(i+1), 'Color', obj.colors.host);
%                             %connection to kink in pipe
%                             line([(obj.offset_codon*0.5 + offset_row -obj.w_codon/2); (obj.offset_codon*max(obj.length_codon_host)); summer_x], [offset_col; offset_col; 0], 'LineWidth', obj.width_mult*x_transfer(i+1), 'Color', obj.colors.host);
%                             text((obj.offset_codon*max(obj.length_codon_host)) - 0.6*obj.offset_codon, offset_col + 0.05, ['\omega_', num2str(r), '=', num2str(x_transfer(i+1), 2)]);
%                             
%                         else
%                             line(obj.offset_codon*0.5 + offset_row +obj.w_codon/2*[-1, 1], [offset_col; offset_col], 'LineWidth', obj.width_mult*x_transfer(i+1), 'Color', obj.colors.host);
%                         end
                     end
                     
                     if (x_transfer_circ(i+1) > 0) %&& (i < rna_curr.N/2)
                         if i == rna_curr.N/2 
                             line([(obj.offset_codon*0.5 + offset_row +obj.w_codon/2*-1); end_terminal_row; summer_x], [offset_col; offset_col; -obj.pool_shift_vert], 'LineWidth', obj.width_mult*x_transfer_circ(i+1), 'Color', [obj.colors.circ, 0.5]);
                         else
                             line(obj.offset_codon*0.5 + offset_row +obj.w_codon/2*[-1, 1], [offset_col; offset_col], 'LineWidth', obj.width_mult*x_transfer_circ(i+1), 'Color', [obj.colors.circ, 0.5]);
                         end
                          
                    end
                    patch(codon(1, :) + offset_row, x_host(i)*codon(2, :) - obj.h_codon/2*(1-x_host(i)) + offset_col, 'b', 'EdgeColor', 'None')
                    patch(codon(1, :) + offset_row, x_circ(i)*codon(2, :) - obj.h_codon/2*(1-x_circ(i)) + offset_col + obj.h_codon*x_host(i), 'r', 'EdgeColor', 'None')
                    patch(codon(1, :) + offset_row, codon(2, :) + offset_col, 'k', 'FaceColor', 'None', 'LineWidth', 2)                                    
                    
                end
                %ribosomes returning to pool
                %outflow = outflow + x_transfer(i+1);
                      
                %flow terminal
                patch((obj.offset_codon*0.5 - obj.offset_codon -obj.w_codon/2) + obj.sum_radius/2 * circ(1, :), offset_col + obj.sum_radius/2 * circ(2, :), 'k', 'FaceColor', [1, 1, 1], 'LineWidth', 1.75)
                patch(end_terminal_row + obj.sum_radius/2 * circ(1, :), offset_col + obj.sum_radius/2 * circ(2, :), 'k', 'FaceColor', [1, 1, 1], 'LineWidth', 1.75)
                
                ind = ind+rna_curr.N;
            end
            
            %pipes for the summer junction
            host_outflow = sum(obj.exit_lambda.*x(obj.exit_codons_host));
            circ_outflow = sum(obj.exit_lambda.*x(obj.exit_codons_circ));
            
            if host_outflow > 0
                line([summer_x; summer_x; obj.pool_shift; obj.pool_shift], obj.h_pool/2 * [0; obj.pool_drop; obj.pool_drop; 1] ,'LineWidth', obj.width_mult*host_outflow, 'Color', obj.colors.host);
            end
            
            if circ_outflow > 0
                line([summer_x; summer_x; obj.pool_shift; obj.pool_shift], -obj.pool_shift_vert -obj.h_pool/2 * [0; obj.pool_drop; obj.pool_drop; 1] ,'LineWidth', obj.width_mult*circ_outflow, 'Color', obj.colors.circ);
            end
            %draw the summer junction
            %if outflow > 0
            %    line([summer_x; summer_x; obj.pool_shift; obj.pool_shift], -obj.h_pool/2 * [0; obj.pool_drop; obj.pool_drop; 1], 'LineWidth', obj.width_mult*outflow, 'Color', obj.colors.host);
            %end
            
            patch(summer_x + obj.sum_radius * circ(1, :), obj.sum_radius * circ(2, :), 'k', 'FaceColor', [1, 1, 1], 'LineWidth', 1.75)
            line(summer_x + 0.6*obj.sum_radius*[-1; 1], [0; 0], 'Color', 'k', 'LineWidth', 1.75)
            line(summer_x * [1; 1], 0.6*obj.sum_radius*[-1; 1], 'Color', 'k', 'LineWidth', 1.75)
            
            patch(summer_x + obj.sum_radius * circ(1, :), obj.sum_radius * circ(2, :)- obj.pool_shift_vert, 'k', 'FaceColor', [1, 1, 1], 'LineWidth', 1.75)
            line(summer_x + 0.6*obj.sum_radius*[-1; 1], [0; 0] - obj.pool_shift_vert, 'Color', 'k', 'LineWidth', 1.75)
            line(summer_x * [1; 1], 0.6*obj.sum_radius*[-1; 1] - obj.pool_shift_vert, 'Color', 'k', 'LineWidth', 1.75)
                                   
            %% draw the pools
            if obj.simple_model
                host_to_circ = z_host * obj.Pool.circ_tag_rate;
                circ_to_host = z_circ * obj.Pool.host_tag_rate;                
                
                host_pipe = (pool(1, 1) - pool(1, 2))*(0.25) - pool(1, 1) + obj.pool_shift;
                circ_pipe = (pool(1, 1) - pool(1, 2))*(0.75) - pool(1, 1) + obj.pool_shift;
                
                if host_to_circ > 0
                    %line([obj.pool_shift - obj.w_pool/2, 1.599*obj.pool_shift- 0.4*obj.w_pool/2, 1.599*obj.pool_shift- 0.4*obj.w_pool/2, obj.pool_shift - obj.w_pool/2],...
                    %    [-0.5*obj.h_pool/2 - obj.pool_shift_vert, -0.5*obj.h_pool/2 - obj.pool_shift_vert, 0.5*obj.h_pool/2, 0.5*obj.h_pool/2], 'Color', obj.colors.host, 'LineWidth', obj.width_mult*host_to_circ)
                    line([host_pipe, host_pipe], [-0.5*obj.h_pool, 0.5*obj.h_pool - obj.pool_shift_vert],  'Color', obj.colors.host, 'LineWidth', obj.width_mult*host_to_circ);
                end
                
                if circ_to_host > 0
                    %line([obj.pool_shift - obj.w_pool/2, 1.599*obj.pool_shift+ 0.8*obj.w_pool/2, 1.599*obj.pool_shift+ 0.8*obj.w_pool/2, obj.pool_shift - obj.w_pool/2],...
                    %    [0.5*obj.h_pool/2 - obj.pool_shift_vert, 0.5*obj.h_pool/2 - obj.pool_shift_vert, -0.5*obj.h_pool/2, -0.5*obj.h_pool/2], 'Color', obj.colors.circ, 'LineWidth', obj.width_mult*host_to_circ)                                
                    line([circ_pipe, circ_pipe], [-0.5*obj.h_pool, 0.5*obj.h_pool - obj.pool_shift_vert],  'Color', obj.colors. circ, 'LineWidth', obj.width_mult*circ_to_host);
                end

                %
%                 patch(pool(1, :) + obj.pool_shift, z_host_scaled*pool(2, :) - obj.h_pool/2*(1-z_host_scaled), 'b', 'EdgeColor', 'None')
%                 patch(pool(1, :) + obj.pool_shift, pool(2, :), 'k', 'FaceColor', 'None', 'LineWidth', 3)                
% 
% 
%                 z_circ_scaled = z_circ/sum(obj.traj.N_r);
%                 patch(pool(1, :) + obj.pool_shift, z_circ_scaled*pool(2, :) - obj.h_pool/2*(1-z_circ_scaled) - obj.pool_shift_vert, 'r', 'EdgeColor', 'None')
%                 patch(pool(1, :) + obj.pool_shift, pool(2, :) - obj.pool_shift_vert, 'k', 'FaceColor', 'None', 'LineWidth', 3)  

                
                z_host_scaled = z_host/sum(obj.traj.N_r);
                patch(pool(1, :) + obj.pool_shift, pool(2, :), 'w', 'LineWidth', 3)                
                patch(pool(1, :) + obj.pool_shift, z_host_scaled*pool(2, :) - obj.h_pool/2*(1-z_host_scaled), 'b', 'EdgeColor', 'None')
                patch(pool(1, :) + obj.pool_shift, pool(2, :), 'k', 'FaceColor', 'None', 'LineWidth', 3)                


                z_circ_scaled = z_circ/sum(obj.traj.N_r);
                patch(pool(1, :) + obj.pool_shift, pool(2, :) - obj.pool_shift_vert, 'w', 'LineWidth', 3)                  
                patch(pool(1, :) + obj.pool_shift, z_circ_scaled*pool(2, :) - obj.h_pool/2*(1-z_circ_scaled) - obj.pool_shift_vert, 'r', 'EdgeColor', 'None')
                patch(pool(1, :) + obj.pool_shift, pool(2, :) - obj.pool_shift_vert, 'k', 'FaceColor', 'None', 'LineWidth', 3)                  
                
                

            else                
                empty_to_host = z_empty * obj.Pool.host_tag_rate;
                empty_to_circ = z_empty * obj.Pool.circ_tag_rate;
                host_to_empty = z_host  * obj.Pool.host_untag_rate;
                circ_to_empty = z_circ  * obj.Pool.circ_untag_rate;

                %host connections
                if empty_to_host > 0
                    line([1.599*obj.pool_shift - 0.4*obj.w_pool/2, 1.599*obj.pool_shift- 0.4*obj.w_pool/2, obj.pool_shift - obj.w_pool/2],...
                        [-obj.pool_shift_vert/2 + obj.h_pool/2, 0.6*obj.h_pool/2, 0.6*obj.h_pool/2], 'Color', obj.colors.empty, 'LineWidth', obj.width_mult*empty_to_host)
                end

                if host_to_empty > 0
                    line([1.599*obj.pool_shift + 0.4*obj.w_pool/2, 1.599*obj.pool_shift + 0.4*obj.w_pool/2, obj.pool_shift - obj.w_pool/2],...
                        [-obj.pool_shift_vert/2 + obj.h_pool/2, 0, 0], 'Color', obj.colors.host, 'LineWidth', obj.width_mult*host_to_empty)
                    
                     line([1.599*obj.pool_shift + 0.4*obj.w_pool/2, 1.599*obj.pool_shift + 0.4*obj.w_pool/2, obj.pool_shift - obj.w_pool/2],...
                        [-obj.pool_shift_vert/2 - obj.h_pool/2,  - obj.pool_shift_vert, - obj.pool_shift_vert], 'Color', obj.colors.circ, 'LineWidth', obj.width_mult*circ_to_empty)

                end

                %circuit connections
                if empty_to_circ > 0
                    if ~isempty(obj.Inhibitor)
                        hill_inhibition = (1/(1 + (x(end)/obj.Inhibitor.p0))^obj.Inhibitor.kf);

                        line([1.599*obj.pool_shift - 0.4*obj.w_pool/2, 1.599*obj.pool_shift- 0.4*obj.w_pool/2, obj.pool_shift - obj.w_pool/2],...
                        [-obj.pool_shift_vert/2 - obj.h_pool/2, -0.6*obj.h_pool/2 - obj.pool_shift_vert, -0.6*obj.h_pool/2 - obj.pool_shift_vert], 'Color', obj.colors.inhibitor, 'LineWidth', obj.width_mult*empty_to_circ)

                        line([1.599*obj.pool_shift - 0.4*obj.w_pool/2, 1.599*obj.pool_shift- 0.4*obj.w_pool/2, obj.pool_shift - obj.w_pool/2],...
                         [-obj.pool_shift_vert/2 - obj.h_pool/2, -0.6*obj.h_pool/2 - obj.pool_shift_vert, -0.6*obj.h_pool/2 - obj.pool_shift_vert], 'Color', obj.colors.empty, 'LineWidth', obj.width_mult*empty_to_circ*hill_inhibition)
                    else
                        line([1.599*obj.pool_shift - 0.4*obj.w_pool/2, 1.599*obj.pool_shift- 0.4*obj.w_pool/2, obj.pool_shift - obj.w_pool/2],...
                        [-obj.pool_shift_vert/2 - obj.h_pool/2, -0.6*obj.h_pool/2 - obj.pool_shift_vert, -0.6*obj.h_pool/2 - obj.pool_shift_vert], 'Color', obj.colors.empty, 'LineWidth', obj.width_mult*empty_to_circ)
                    end

                end

                if circ_to_empty > 0
                    line([1.599*obj.pool_shift + 0.4*obj.w_pool/2, 1.599*obj.pool_shift + 0.4*obj.w_pool/2, obj.pool_shift - obj.w_pool/2],...
                        [-obj.pool_shift_vert/2 - obj.h_pool/2,  - obj.pool_shift_vert, - obj.pool_shift_vert], 'Color', obj.colors.circ, 'LineWidth', obj.width_mult*circ_to_empty)
                end




                z_host_scaled = z_host/sum(obj.traj.N_r);
                patch(pool(1, :) + obj.pool_shift, z_host_scaled*pool(2, :) - obj.h_pool/2*(1-z_host_scaled), 'b', 'EdgeColor', 'None')
                patch(pool(1, :) + obj.pool_shift, pool(2, :), 'k', 'FaceColor', 'None', 'LineWidth', 3)                


                z_circ_scaled = z_circ/sum(obj.traj.N_r);
                patch(pool(1, :) + obj.pool_shift, z_circ_scaled*pool(2, :) - obj.h_pool/2*(1-z_circ_scaled) - obj.pool_shift_vert, 'r', 'EdgeColor', 'None')
                patch(pool(1, :) + obj.pool_shift, pool(2, :) - obj.pool_shift_vert, 'k', 'FaceColor', 'None', 'LineWidth', 3)  

                z_empty_scaled = z_empty/sum(obj.traj.N_r);
                patch(pool(1, :) + 1.599*obj.pool_shift, z_empty_scaled*pool(2, :) - obj.h_pool/2*(1-z_empty_scaled) - obj.pool_shift_vert/2, [148,0,211]/255, 'EdgeColor', 'None')

                patch(pool(1, :) + 1.599*obj.pool_shift, pool(2, :) - obj.pool_shift_vert/2, 'k', 'FaceColor', 'None', 'LineWidth', 3)  
            end

            %done with plotting
            axis image; 
            axis(obj.axis_limit);
            title(['N_r = ', num2str(sum(obj.traj.N_r)), ', t = ', num2str(t(1),'%.2f') ' sec']);

            set(gca,'XTick',[],'YTick',[])
            set(gca,'XColor','none','YColor','none')

            drawnow;
        end    
        
        function M = playback(obj, filename)
            %run the animation
            
            if (nargin < 2)
                record = 0;
            else
                record = 1;
            end
            
            t_min = obj.traj.spline.breaks(1);
            t_max = obj.traj.spline.breaks(end);
            
            t = t_min:obj.sample_rate:t_max;            
            
            x_interp = ppval(obj.traj.spline, t);
            
            %set up folder
            if record && (exist(['img/', filename], 'dir') == 0)
                mkdir(['img/', filename]);
            end
            
            for i = 1:length(t)
                t_curr = t(i);
                x_curr = x_interp(:, i);
                
                obj.draw(t_curr, x_curr);
                
                %print out the images
                if record
                    print(['img/', filename, '/Frame ' num2str(i)], '-dpng', '-r150');
                end
            end
            
            if record
                %save the output
                GifName = strcat('img/gif_out/', filename, '.gif');
                delay = obj.sample_rate;
                for i = 1:length(t)
                    [A, ~] = imread(['img/', filename, '/Frame ', num2str(i), '.png']);
                    [X, map] = rgb2ind(A, 256);
                    if i == 1
                        imwrite(X, map, GifName, 'gif', 'LoopCount', inf, 'DelayTime', delay)
                    else
                        imwrite(X, map, GifName, 'gif', 'WriteMode', 'append', 'DelayTime', delay)
                    end
                end
            end
        end
    end
  
end
