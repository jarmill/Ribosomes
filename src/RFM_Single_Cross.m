classdef RFM_Single_Cross
    %RFM_Single_Cross A single strand of Cross RFM  with N different
    %ribosomal species. 
    
    properties
        lambda;         %flow rates 
        N_states;       %number of variables
        N_species;      %number of ribosomal species
        N_codons;       %number of codons in RFM
        
        traj;           %trajectories
    end
    
    methods
        function obj = RFM_Single_Cross(lambda)
            %RFM_Single_Cross Construct an instance of this class
            %   load up the constants
            %obj.Property1 = inputArg1 + inputArg2;
            
            obj.lambda =  lambda;
            %[obj.N_species, obj.N_codons] = size(lambda);
            obj.N_species = size(lambda, 1); 
            obj.N_codons  = size(lambda, 2) - 1;
            obj.N_states  = obj.N_species*obj.N_codons;
        end
        
%         function outputArg = method1(obj,inputArg)
%             %METHOD1 Summary of this method goes here
%             %   Detailed explanation goes here
%             outputArg = obj.Property1 + inputArg;
%         end     
  
        function [flows, xdot] = rna_flow(obj, x)
            %RNA_FLOW: finds the change in ribosome content in a single strand
            %
            %Input:
            %   x:      Number of ribosomes in each species
            %
            %Output: 
            %   flows:  Total flow in ribosomes from one codon to another
            %   xdot:   Change in ribosomes for all codons for RNA
            X = reshape(x, [obj.N_species, obj.N_codons]);  
            z = zeros(obj.N_species, 1);
            
            sX = sum(X, 1); 
            
            X_send = [1-z X];
            X_recieve = repmat([1-sX 1], [obj.N_species, 1]);  
            
            flows = obj.lambda .* X_send .* X_recieve;
            
            Xdot = flows(:, 1:end-1) - flows(:, 2:end);
            
            xdot = reshape(Xdot, [obj.N_states, 1]);
            
        end
        
        function xdot = dynamics(obj, t, x)
            %function to perform derivatives
            [~, xdot] =  obj.rna_flow(x);
        end
        
        function obj = simulate(obj, t_max)
            traj_out = struct;
            
            t_range = [0 t_max];
            x0 = zeros(obj.N_states, 1);
            
            [T, X] = ode45(@obj.dynamics, t_range, x0, []);
            
            
            traj_out.t = T;
            traj_out.x = X;  
            
            
            %protein output
            lambda_last = obj.lambda(:, end);
            X_last = X(:, (end-obj.N_species+1): end);
            traj_out.y = X_last * lambda_last;
            
            
            traj_out.spline  = spline(T, X');                                                                                                              
            obj.traj = traj_out;
        end
    end
end

