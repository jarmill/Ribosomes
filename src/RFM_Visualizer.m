classdef RFM_Visualizer
% Implements the draw function for the Flow Engine model
% based on heave dynamics of an air cushioned vehicle

    properties
        %trajectory parameters
        sample_rate;
        traj;
        RFM;
        
        %indexing
        entry_codons;
        exit_codons;
        length_codon;
        
        %visualization parameters
        w_codon = 0.1;
        h_codon = 0.1;
        offset_codon = 0.2;
        offset_rna = 0.2;
        axis_limit;
        title = 0;
        
        sum_radius = 0.05;
        
        width_mult = 4;
        
        w_pool = 0.4;
        h_pool = 0.8;
        pool_shift = -0.5;
        pool_drop = 1.4;
    end

    methods
        function obj = RFM_Visualizer(plant, sample_rate)
            %typecheck(plant,'RFM_Network');
            obj.RFM = plant.RFM;
            obj.traj = plant.traj;
            
            %shortcuts here
            obj.entry_codons = plant.entry_codons;            
            obj.exit_codons  = plant.exit_codons;
            obj.length_codon = plant.length_codon;
            
            obj.axis_limit = [-0.7 (obj.offset_codon*max(obj.length_codon) + 0.3) -0.8 0.7];
            
            if nargin < 2
                obj.sample_rate = 0.05;
            else
                obj.sample_rate = sample_rate;
            end
        end
    
        function draw(obj,t,x)
            % Draw the Ribosomal Flow Network
            %       persistent hFig skate bag;
            persistent hFig codon pool circ;
      
            if (isempty(hFig) || ~isvalid(hFig))
                %hFig = sfigure(25);
                %set(hFig,'DoubleBuffer', 'on');
                hFig = figure(25);
                
                codon = [obj.w_codon/2*[1 -1 -1 1]; obj.h_codon/2*[1 1 -1 -1]];
                pool = [obj.w_pool/2*[1 -1 -1 1]; obj.h_pool/2*[1 1 -1 -1]];
                th = linspace(0, 2*pi, 150);
                circ = [cos(th); sin(th)];
            end
            
            figure(hFig); cla; hold on; view(0,90);
            %sfigure(hFig); cla; hold on; view(0,90);
%       %patch(x(1)+p(1,:), x(2)+p(2,:),1+0*p(1,:),'b','FaceColor',[0 0 0])
            z = x(1);
            pool_edge = obj.w_pool/2 - 0.5;
            
            %draw the RNA
            N_rna = length(obj.RFM);
            
            %handle the column offsets
            possible_col = linspace(obj.h_pool/2, -obj.h_pool/2, N_rna + 2);
            col_list = possible_col(2:end-1);
            
            outflow = 0;
            for r = 1:N_rna
                %get the current states along the RNA
                offset_col = col_list(r);
                rna_curr = obj.RFM{r};
                x_curr = x(obj.entry_codons(r) : obj.exit_codons(r));
                
                %find the flow 
                r_in = tanh(z);
                x_stagger = [r_in; x_curr; 0];
                x_transfer = rna_curr.lambda .* x_stagger(1:end-1) .* (1-x_stagger(2:end));

                %summing junction setup, centered at (summer_x, 0)
                summer_x = (obj.offset_codon*max(obj.length_codon) + 3*obj.sum_radius);
            
                %transfer from pool
                if x_transfer(1) > 0
                    line([pool_edge; -obj.w_codon/2], [offset_col; offset_col], 'LineWidth', obj.width_mult*x_transfer(1), 'Color', [78,206,255]/255);
                end
                
                
               
                for i = 1:rna_curr.N
                    %plot position of each codon
                    offset_row = obj.offset_codon*(i-1);
                    if x_transfer(i+1) > 0
                        if i == rna_curr.N
                            %blah
                            %line(obj.offset_codon*0.5 + offset_row +obj.w_codon/2*[-1, 1], [offset_col; offset_col], 'LineWidth', obj.width_mult*x_transfer(i+1), 'Color', [78,206,255]/255);
                            %connection to kink in pipe
                            line([(obj.offset_codon*0.5 + offset_row -obj.w_codon/2); (obj.offset_codon*max(obj.length_codon)); summer_x], [offset_col; offset_col; 0], 'LineWidth', obj.width_mult*x_transfer(i+1), 'Color', [78,206,255]/255);
                            text((obj.offset_codon*max(obj.length_codon)) - 0.6*obj.offset_codon, offset_col + 0.05, ['y_', num2str(r), '= ', num2str(x_transfer(i+1), 3)]);
                            
                        else
                            line(obj.offset_codon*0.5 + offset_row +obj.w_codon/2*[-1, 1], [offset_col; offset_col], 'LineWidth', obj.width_mult*x_transfer(i+1), 'Color', [78,206,255]/255);
                        end
                    end
                    patch(codon(1, :) + offset_row, x_curr(i)*codon(2, :) - obj.h_codon/2*(1-x_curr(i)) + offset_col, 'b', 'EdgeColor', 'None')
                    patch(codon(1, :) + offset_row, codon(2, :) + offset_col, 'k', 'FaceColor', 'None', 'LineWidth', 2)                
                    

                end
                %ribosomes returning to pool
                outflow = outflow + x_transfer(i+1);
                            
            end
            
            %pipes for the summer junction
            
            %draw the summer junction
            if outflow > 0
                line([summer_x; summer_x; obj.pool_shift; obj.pool_shift], -obj.h_pool/2 * [0; obj.pool_drop; obj.pool_drop; 1], 'LineWidth', obj.width_mult*outflow, 'Color', [78,206,255]/255);

            end
            patch(summer_x + obj.sum_radius * circ(1, :), obj.sum_radius * circ(2, :), 'k', 'FaceColor', [1, 1, 1], 'LineWidth', 1.75)
            line(summer_x + 0.6*obj.sum_radius*[-1; 1], [0; 0], 'Color', 'k', 'LineWidth', 1.75)
            line(summer_x * [1; 1], 0.6*obj.sum_radius*[-1; 1], 'Color', 'k', 'LineWidth', 1.75)
            
            %draw the pool
            z_scaled = z/obj.traj.N_r;
            patch(pool(1, :) + obj.pool_shift, z_scaled*pool(2, :) - obj.h_pool/2*(1-z_scaled), 'b', 'EdgeColor', 'None')
            patch(pool(1, :) + obj.pool_shift, pool(2, :), 'k', 'FaceColor', 'None', 'LineWidth', 3)                

            %done with plotting
            axis image; 
            axis(obj.axis_limit);
            %scatter(cos(t), sin(t))
            if obj.title 
                title(['N_r = ', num2str(sum(obj.traj.N_r)), ', t = ', num2str(t(1),'%.2f') ' sec']);
            end
            
            set(gca,'XTick',[],'YTick',[])
            set(gca,'XColor','none','YColor','none')

            drawnow;
        end    
        
        function M = playback(obj, filename)
            %run the animation
            
            if (nargin < 2)
                record = 0;
            else
                record = 1;
            end
            
            t_min = obj.traj.spline.breaks(1);
            t_max = obj.traj.spline.breaks(end);
            
            t = t_min:obj.sample_rate:t_max;            
            
            x_interp = ppval(obj.traj.spline, t);
            
            %set up folder
            if record && (exist(['img/', filename], 'dir') == 0)
                mkdir(['img/', filename]);
            end
            
            for i = 1:length(t)
                t_curr = t(i);
                x_curr = x_interp(:, i);
                
                obj.draw(t_curr, x_curr);
                
                %print out the images
                if record
                    print(['img/', filename, '/Frame ' num2str(i)], '-dpng', '-r150');
                end
            end
            
            if record
                %save the output
                GifName = strcat('img/gif_out/', filename, '.gif');
                delay = obj.sample_rate;
                for i = 1:length(t)
                    [A, ~] = imread(['img/', filename, '/Frame ', num2str(i), '.png']);
                    [X, map] = rgb2ind(A, 256);
                    if i == 1
                        imwrite(X, map, GifName, 'gif', 'LoopCount', inf, 'DelayTime', delay)
                    else
                        imwrite(X, map, GifName, 'gif', 'WriteMode', 'append', 'DelayTime', delay)
                    end
                end
            end
        end
    end
  
end
