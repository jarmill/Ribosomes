classdef RFM_Split_Visualizer
% Implements the draw function for the Flow Engine model
% based on heave dynamics of an air cushioned vehicle

    properties
        %trajectory parameters
        sample_rate;
        traj;
        RFM_host;
        RFM_circ;
        simple_model;
        Inhibitor;
        Pool;
        
        %indexing
        entry_codons_host;
        exit_codons_host;
        length_codon_host;
        
        entry_codons_circ;
        exit_codons_circ;
        length_codon_circ;
        
        %visualization parameters
        w_codon = 0.1;
        h_codon = 0.1;
        offset_codon = 0.2;
        offset_rna = 0.2;
        axis_limit;
        
        sum_radius = 0.05;
        
        width_mult = 4;
        
        w_pool = 0.4;
        h_pool = 0.8;
        pool_shift = -0.5;
        pool_shift_vert = 1.5;
        pool_drop = 1.4;
    end

    methods
        function obj = RFM_Split_Visualizer(plant, sample_rate)
            obj.RFM_circ = plant.RFM_circ;
            obj.RFM_host= plant.RFM_host;
            obj.simple_model = plant.simple_model;
            obj.Inhibitor = plant.Inhibitor;
            obj.Pool = plant.Pool;
            obj.traj = plant.traj;
            obj.pool_shift_vert = 1.5*obj.h_pool;
            
            %shortcuts here
            obj.entry_codons_host = plant.entry_codons_host;            
            obj.exit_codons_host  = plant.exit_codons_host;
            obj.length_codon_host = plant.length_codon_host;
            
            obj.entry_codons_circ = plant.entry_codons_circ;            
            obj.exit_codons_circ  = plant.exit_codons_circ;
            obj.length_codon_circ = plant.length_codon_circ;
            
            
            %obj.axis_limit = [-0.7 (obj.offset_codon*max([obj.length_codon_host obj.length_codon_circ]) + 0.3) -0.8-obj.pool_shift_vert 0.8];
            obj.axis_limit = [-1.5 (obj.offset_codon*max([obj.length_codon_host obj.length_codon_circ]) + 0.5) ...
                -0.8-obj.pool_shift_vert 0.8];
            
            if nargin < 2
                obj.sample_rate = 0.05;
            else
                obj.sample_rate = sample_rate;
            end
        end
    
        function draw(obj,t,x)
            % Draw the Ribosomal Flow Network
            %       persistent hFig skate bag;
            persistent hFig codon pool circ;
      
            if (isempty(hFig) || ~isvalid(hFig))
                hFig = figure(25);
                set(hFig,'DoubleBuffer', 'on');
                
                codon = [obj.w_codon/2*[1 -1 -1 1]; obj.h_codon/2*[1 1 -1 -1]];
                pool = [obj.w_pool/2*[1 -1 -1 1]; obj.h_pool/2*[1 1 -1 -1]];
                th = linspace(0, 2*pi, 150);
                circ = [cos(th); sin(th)];
            end
            
            figure(hFig); cla; hold on; view(0,90);

            %extract data
            if obj.simple_model
                z_host = x(1);
                z_circ = x(2);
            else
                z_empty = x(1);
                z_host  = x(2);
                z_circ  = x(3);
            end
            
            x_host = x(obj.entry_codons_host(1):obj.exit_codons_host(end));
            if isempty(obj.RFM_circ)
                x_circ = [];
            else
                x_circ = x(obj.entry_codons_circ(1):obj.exit_codons_circ(end));
            end

            pool_edge = obj.w_pool/2 - 0.5;
            
            %% draw the RNA (host)
            N_rna_host = length(obj.RFM_host);
            
            %handle the column offsets
            possible_col = linspace(obj.h_pool/2, -obj.h_pool/2, N_rna_host + 2);
            col_list = possible_col(2:end-1);
            
            outflow = 0;
            ind = 1;
            for r = 1:N_rna_host
                %get the current states along the RNA
                offset_col = col_list(r);
                rna_curr = obj.RFM_host{r};
                x_curr = x_host(ind : ind+rna_curr.N-1);
                
                %find the flow 
                r_in = rna_curr.G(z_host);
                x_stagger = [r_in; x_curr; 0];
                x_transfer = rna_curr.lambda .* x_stagger(1:end-1) .* (1-x_stagger(2:end));

                %summing junction setup, centered at (summer_x, 0)
                summer_x = (obj.offset_codon*max(obj.length_codon_host) + 2*obj.sum_radius);                
%                 summer_x = (obj.offset_codon*max(obj.length_codon_host) + 3*obj.sum_radius);                
%                 summer_x = (obj.offset_codon*max(obj.length_codon_host) + 6*obj.sum_radius);
            
                %transfer from pool
                if x_transfer(1) > 0
                    line([pool_edge; -obj.w_codon/2], [offset_col; offset_col], 'LineWidth', obj.width_mult*x_transfer(1), 'Color', [78,206,255]/255);
                end
                
                
               
                for i = 1:rna_curr.N
                    %plot position of each codon
                    offset_row = obj.offset_codon*(i-1);
                    if x_transfer(i+1) > 0
                        if i == rna_curr.N
                            %blah
                            %line(obj.offset_codon*0.5 + offset_row +obj.w_codon/2*[-1, 1], [offset_col; offset_col], 'LineWidth', obj.width_mult*x_transfer(i+1), 'Color', [78,206,255]/255);
                            %connection to kink in pipe
                            line([(obj.offset_codon*0.5 + offset_row -obj.w_codon/2); (obj.offset_codon*max(obj.length_codon_host)); summer_x], [offset_col; offset_col; 0], 'LineWidth', obj.width_mult*x_transfer(i+1), 'Color', [78,206,255]/255);
%                             text((obj.offset_codon*max(obj.length_codon_host)) - 0.6*obj.offset_codon, ...
                            text((obj.offset_codon*max(obj.length_codon_host)) + 2*obj.w_codon, ...
                                offset_col + 0.05, ['y_', num2str(r), '=', num2str(x_transfer(i+1), 3)]);
                            
                        else
                            line(obj.offset_codon*0.5 + offset_row +obj.w_codon/2*[-1, 1], [offset_col; offset_col], 'LineWidth', obj.width_mult*x_transfer(i+1), 'Color', [78,206,255]/255);
                        end
                    end
                    patch(codon(1, :) + offset_row, x_curr(i)*codon(2, :) - obj.h_codon/2*(1-x_curr(i)) + offset_col, 'b', 'EdgeColor', 'None')
                    patch(codon(1, :) + offset_row, codon(2, :) + offset_col, 'k', 'FaceColor', 'None', 'LineWidth', 2)                
                    

                end
                %ribosomes returning to pool
                outflow = outflow + x_transfer(i+1);
                      
                ind = ind+rna_curr.N;
            end
            
            %pipes for the summer junction
            
            %draw the summer junction
            if N_rna_host
                if outflow > 0
                    L_host = line([summer_x; summer_x; obj.pool_shift; obj.pool_shift], -obj.h_pool/2 * [0; obj.pool_drop; obj.pool_drop; 1], 'LineWidth', obj.width_mult*outflow, 'Color', [78,206,255]/255);
                    uistack(L_host, 'bottom');
                end
                patch(summer_x + obj.sum_radius * circ(1, :), obj.sum_radius * circ(2, :), 'k', 'FaceColor', [1, 1, 1], 'LineWidth', 1.75)
                line(summer_x + 0.6*obj.sum_radius*[-1; 1], [0; 0], 'Color', 'k', 'LineWidth', 1.75)
                line(summer_x * [1; 1], 0.6*obj.sum_radius*[-1; 1], 'Color', 'k', 'LineWidth', 1.75)
            end
            %% draw the RNA (circ)
            N_rna_circ = length(obj.RFM_circ);
            
            %handle the column offsets
            possible_col = linspace(obj.h_pool/2, -obj.h_pool/2, N_rna_circ + 2);
            col_list = possible_col(2:end-1);
            
            outflow = 0;
            ind = 1;
            for r = 1:N_rna_circ
                %get the current states along the RNA
                offset_col = col_list(r);
                rna_curr = obj.RFM_circ{r};
                x_curr = x_circ(ind : ind+rna_curr.N-1);
                
                %find the flow 
                r_in = rna_curr.G(z_circ);
                x_stagger = [r_in; x_curr; 0];
                x_transfer = rna_curr.lambda .* x_stagger(1:end-1) .* (1-x_stagger(2:end));

                %summing junction setup, centered at (summer_x, 0)
%                 summer_x = (obj.offset_codon*max(obj.length_codon_circ) + 6*obj.sum_radius);
                summer_x = (obj.offset_codon*max(obj.length_codon_circ) + 2*obj.sum_radius);                
            
                %transfer from pool
                if x_transfer(1) > 0
                    line([pool_edge; -obj.w_codon/2], [offset_col; offset_col] - obj.pool_shift_vert, 'LineWidth', obj.width_mult*x_transfer(1), 'Color', [247,120,120]/255);
                end
                
                
               
                for i = 1:rna_curr.N
                    %plot position of each codon
                    offset_row = obj.offset_codon*(i-1);
                    if x_transfer(i+1) > 0
                        if i == rna_curr.N
                            %blah
                            %line(obj.offset_codon*0.5 + offset_row +obj.w_codon/2*[-1, 1], [offset_col; offset_col], 'LineWidth', obj.width_mult*x_transfer(i+1), 'Color', [78,206,255]/255);
                            %connection to kink in pipe
                            line([(obj.offset_codon*0.5 + offset_row -obj.w_codon/2); (obj.offset_codon*max(obj.length_codon_circ)); summer_x], [offset_col; offset_col; 0]- obj.pool_shift_vert, 'LineWidth', obj.width_mult*x_transfer(i+1), 'Color', [247,120,120]/255);
%                             text((obj.offset_codon*max(obj.length_codon_circ)) - 0.6*obj.offset_codon, ...
                            text((obj.offset_codon*max(obj.length_codon_circ)) + 2*obj.w_codon, ...
                                offset_col + 0.05 - obj.pool_shift_vert, ['y_', num2str(r), '=', num2str(x_transfer(i+1), 3)]);
                        else
                            line(obj.offset_codon*0.5 + offset_row +obj.w_codon/2*[-1, 1], [offset_col; offset_col]- obj.pool_shift_vert, 'LineWidth', obj.width_mult*x_transfer(i+1), 'Color', [247,120,120]/255);
                        end
                    end
                    patch(codon(1, :) + offset_row, x_curr(i)*codon(2, :) - obj.h_codon/2*(1-x_curr(i)) + offset_col- obj.pool_shift_vert, 'r', 'EdgeColor', 'None')
                    
                                                            
                    if ~isempty(obj.Inhibitor) && (r==1)
                        patch(codon(1, :) + offset_row, codon(2, :) + offset_col - obj.pool_shift_vert, 'k', 'EdgeColor', [218,165,32]/255, 'FaceColor', 'None', 'LineWidth', 2)                
                    else
                        patch(codon(1, :) + offset_row, codon(2, :) + offset_col - obj.pool_shift_vert, 'k', 'FaceColor', 'None', 'LineWidth', 2)
                    end

                end
                %ribosomes returning to pool
                outflow = outflow + x_transfer(i+1);
                      
                ind = ind+rna_curr.N;
            end
            
            %pipes for the summer junction
            
            %draw the summer junction
            if N_rna_circ
                if outflow > 0
                    L_circ = line([summer_x; summer_x; obj.pool_shift; obj.pool_shift], -obj.h_pool/2 * [0; obj.pool_drop; obj.pool_drop; 1] - obj.pool_shift_vert, 'LineWidth', obj.width_mult*outflow, 'Color', [247,120,120]/255);
                    uistack(L_circ, 'bottom');
                end
                patch(summer_x + obj.sum_radius * circ(1, :), obj.sum_radius * circ(2, :) - obj.pool_shift_vert, 'k', 'FaceColor', [1, 1, 1], 'LineWidth', 1.75)
                line(summer_x + 0.6*obj.sum_radius*[-1; 1], [0; 0] - obj.pool_shift_vert, 'Color', 'k', 'LineWidth', 1.75)
                line(summer_x * [1; 1], 0.6*obj.sum_radius*[-1; 1] - obj.pool_shift_vert, 'Color', 'k', 'LineWidth', 1.75)
            end
            
            %% draw the pools
            if obj.simple_model
                host_to_circ = z_host * obj.Pool.circ_tag_rate;
                circ_to_host = z_circ * obj.Pool.host_tag_rate;                
                
                if host_to_circ > 0
                    %line([obj.pool_shift - obj.w_pool/2, 2.25*obj.pool_shift- 0.4*obj.w_pool/2, 2.25*obj.pool_shift - 0.4*obj.w_pool/2, 2.25*obj.pool_shift- 0.4*obj.w_pool/2, obj.pool_shift - obj.w_pool/2],...
                    %    [-0.6*obj.h_pool/2 - obj.pool_shift_vert, -0.6*obj.h_pool/2 - obj.pool_shift_vert, -obj.pool_shift_vert/2 + obj.h_pool/2, 0.6*obj.h_pool/2, 0.6*obj.h_pool/2], 'Color', [78,206,255]/255, 'LineWidth', obj.width_mult*host_to_circ)                    
                    line([obj.pool_shift - obj.w_pool/2, 2.25*obj.pool_shift- 0.4*obj.w_pool/2, 2.25*obj.pool_shift- 0.4*obj.w_pool/2, obj.pool_shift - obj.w_pool/2],...
                        [-0.5*obj.h_pool/2 - obj.pool_shift_vert, -0.5*obj.h_pool/2 - obj.pool_shift_vert, 0.5*obj.h_pool/2, 0.5*obj.h_pool/2], 'Color', [78,206,255]/255, 'LineWidth', obj.width_mult*host_to_circ)
                end
                
                if circ_to_host > 0
                    line([obj.pool_shift - obj.w_pool/2, 2.25*obj.pool_shift+ 0.8*obj.w_pool/2, 2.25*obj.pool_shift+ 0.8*obj.w_pool/2, obj.pool_shift - obj.w_pool/2],...
                        [0.5*obj.h_pool/2 - obj.pool_shift_vert, 0.5*obj.h_pool/2 - obj.pool_shift_vert, -0.5*obj.h_pool/2, -0.5*obj.h_pool/2], 'Color', [247,120,120]/255, 'LineWidth', obj.width_mult*host_to_circ)                    
                    
                    %line([obj.pool_shift - obj.w_pool/2, 2.25*obj.pool_shift- 0.4*obj.w_pool/2, 2.25*obj.pool_shift - 0.4*obj.w_pool/2, 2.25*obj.pool_shift- 0.4*obj.w_pool/2, obj.pool_shift - obj.w_pool/2],...
                    %    [0.6*obj.h_pool/2 - obj.pool_shift_vert, 0.6*obj.h_pool/2 - obj.pool_shift_vert, -obj.pool_shift_vert/2 + obj.h_pool/2, -0.6*obj.h_pool/2, -0.6*obj.h_pool/2], 'Color', [247,120,120]/255, 'LineWidth', obj.width_mult*host_to_circ)                    

                end
                
                z_host_scaled = z_host/sum(obj.traj.N_r);
                patch(pool(1, :) + obj.pool_shift, z_host_scaled*pool(2, :) - obj.h_pool/2*(1-z_host_scaled), 'b', 'EdgeColor', 'None')
                patch(pool(1, :) + obj.pool_shift, pool(2, :), 'k', 'FaceColor', 'None', 'LineWidth', 3)                


                z_circ_scaled = z_circ/sum(obj.traj.N_r);
                patch(pool(1, :) + obj.pool_shift, z_circ_scaled*pool(2, :) - obj.h_pool/2*(1-z_circ_scaled) - obj.pool_shift_vert, 'r', 'EdgeColor', 'None')
                patch(pool(1, :) + obj.pool_shift, pool(2, :) - obj.pool_shift_vert, 'k', 'FaceColor', 'None', 'LineWidth', 3)                  
                
                

            else                
                empty_to_host = z_empty * obj.Pool.host_tag_rate;
                empty_to_circ = z_empty * obj.Pool.circ_tag_rate;
                host_to_empty = z_host  * obj.Pool.host_untag_rate;
                circ_to_empty = z_circ  * obj.Pool.circ_untag_rate;

                %host connections
                if empty_to_host > 0
                    line([2.25*obj.pool_shift - 0.4*obj.w_pool/2, 2.25*obj.pool_shift- 0.4*obj.w_pool/2, obj.pool_shift - obj.w_pool/2],...
                        [-obj.pool_shift_vert/2 + obj.h_pool/2, 0.6*obj.h_pool/2, 0.6*obj.h_pool/2], 'Color', [216,191,216]/255, 'LineWidth', obj.width_mult*empty_to_host)
                end

                if host_to_empty > 0
                    line([2.25*obj.pool_shift + 0.4*obj.w_pool/2, 2.25*obj.pool_shift + 0.4*obj.w_pool/2, obj.pool_shift - obj.w_pool/2],...
                        [-obj.pool_shift_vert/2 + obj.h_pool/2, 0, 0], 'Color', [78,206,255]/255, 'LineWidth', obj.width_mult*host_to_empty)
                    
                     line([2.25*obj.pool_shift + 0.4*obj.w_pool/2, 2.25*obj.pool_shift + 0.4*obj.w_pool/2, obj.pool_shift - obj.w_pool/2],...
                        [-obj.pool_shift_vert/2 - obj.h_pool/2,  - obj.pool_shift_vert, - obj.pool_shift_vert], 'Color', [247,120,120]/255, 'LineWidth', obj.width_mult*circ_to_empty)

                end

                %circuit connections
                if empty_to_circ > 0
                    if ~isempty(obj.Inhibitor)
                        hill_inhibition = (1/(1 + (x(end)/obj.Inhibitor.p0))^obj.Inhibitor.kf);

                        line([2.25*obj.pool_shift - 0.4*obj.w_pool/2, 2.25*obj.pool_shift- 0.4*obj.w_pool/2, obj.pool_shift - obj.w_pool/2],...
                        [-obj.pool_shift_vert/2 - obj.h_pool/2, -0.6*obj.h_pool/2 - obj.pool_shift_vert, -0.6*obj.h_pool/2 - obj.pool_shift_vert], 'Color', [238,232,170]/255, 'LineWidth', obj.width_mult*empty_to_circ)

                        line([2.25*obj.pool_shift - 0.4*obj.w_pool/2, 2.25*obj.pool_shift- 0.4*obj.w_pool/2, obj.pool_shift - obj.w_pool/2],...
                         [-obj.pool_shift_vert/2 - obj.h_pool/2, -0.6*obj.h_pool/2 - obj.pool_shift_vert, -0.6*obj.h_pool/2 - obj.pool_shift_vert], 'Color', [216,191,216]/255, 'LineWidth', obj.width_mult*empty_to_circ*hill_inhibition)
                    else
                        line([2.25*obj.pool_shift - 0.4*obj.w_pool/2, 2.25*obj.pool_shift- 0.4*obj.w_pool/2, obj.pool_shift - obj.w_pool/2],...
                        [-obj.pool_shift_vert/2 - obj.h_pool/2, -0.6*obj.h_pool/2 - obj.pool_shift_vert, -0.6*obj.h_pool/2 - obj.pool_shift_vert], 'Color', [216,191,216]/255, 'LineWidth', obj.width_mult*empty_to_circ)
                    end

                end

                if circ_to_empty > 0
                    line([2.25*obj.pool_shift + 0.4*obj.w_pool/2, 2.25*obj.pool_shift + 0.4*obj.w_pool/2, obj.pool_shift - obj.w_pool/2],...
                        [-obj.pool_shift_vert/2 - obj.h_pool/2,  - obj.pool_shift_vert, - obj.pool_shift_vert], 'Color', [247,120,120]/255, 'LineWidth', obj.width_mult*circ_to_empty)
                end




                z_host_scaled = z_host/sum(obj.traj.N_r);
                patch(pool(1, :) + obj.pool_shift, z_host_scaled*pool(2, :) - obj.h_pool/2*(1-z_host_scaled), 'b', 'EdgeColor', 'None')
                patch(pool(1, :) + obj.pool_shift, pool(2, :), 'k', 'FaceColor', 'None', 'LineWidth', 3)                


                z_circ_scaled = z_circ/sum(obj.traj.N_r);
                patch(pool(1, :) + obj.pool_shift, z_circ_scaled*pool(2, :) - obj.h_pool/2*(1-z_circ_scaled) - obj.pool_shift_vert, 'r', 'EdgeColor', 'None')
                patch(pool(1, :) + obj.pool_shift, pool(2, :) - obj.pool_shift_vert, 'k', 'FaceColor', 'None', 'LineWidth', 3)  

                z_empty_scaled = z_empty/sum(obj.traj.N_r);
                patch(pool(1, :) + 2.25*obj.pool_shift, z_empty_scaled*pool(2, :) - obj.h_pool/2*(1-z_empty_scaled) - obj.pool_shift_vert/2, [148,0,211]/255, 'EdgeColor', 'None')

                patch(pool(1, :) + 2.25*obj.pool_shift, pool(2, :) - obj.pool_shift_vert/2, 'k', 'FaceColor', 'None', 'LineWidth', 3)  
            end

            %done with plotting
            axis image; 
            axis(obj.axis_limit);
            %scatter(cos(t), sin(t))
            %title(['t = ', num2str(t(1),'%.2f') ' sec']);
            title(['N_r = ', num2str(sum(obj.traj.N_r)), ', t = ', num2str(t(1),'%.2f') ' sec']);

            set(gca,'XTick',[],'YTick',[])
            set(gca,'XColor','none','YColor','none')

            drawnow;
        end    
        
        function M = playback(obj, filename)
            %run the animation
            
            if (nargin < 2)
                record = 0;
            else
                record = 1;
            end
            
            t_min = obj.traj.spline.breaks(1);
            t_max = obj.traj.spline.breaks(end);
            
            t = t_min:obj.sample_rate:t_max;            
            
            x_interp = ppval(obj.traj.spline, t);
            
            %set up folder
            if record && (exist(['img/', filename], 'dir') == 0)
                mkdir(['img/', filename]);
            end
            
            for i = 1:length(t)
                t_curr = t(i);
                x_curr = x_interp(:, i);
                
                obj.draw(t_curr, x_curr);
                
                %print out the images
                if record
                    print(['img/', filename, '/Frame ' num2str(i)], '-dpng', '-r150');
                end
            end
            
            if record
                %save the output
                GifName = strcat('img/gif_out/', filename, '.gif');
                delay = obj.sample_rate;
                for i = 1:length(t)
                    [A, ~] = imread(['img/', filename, '/Frame ', num2str(i), '.png']);
                    [X, map] = rgb2ind(A, 256);
                    if i == 1
                        imwrite(X, map, GifName, 'gif', 'LoopCount', inf, 'DelayTime', delay)
                    else
                        imwrite(X, map, GifName, 'gif', 'WriteMode', 'append', 'DelayTime', delay)
                    end
                end
            end
        end
    end
  
end
