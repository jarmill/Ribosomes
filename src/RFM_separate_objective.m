function y_tot = RFM_separate_objective(RFM, w, N)
    
if any(isnan(N))
    y_tot = 0;
else
    N_species = length(RFM);
    Count = 0;
    y_tot = 0;
    for i = 1:N_species
        wi = w(Count + 1:RFM{i}.N_strands);
        Ni = N(i);
        yi = RFM{i}.steady_state_output(wi, Ni);
        y_tot = yi + y_tot;
		Count = Count + RFM{i}.N_strands  ;
    end
end

end