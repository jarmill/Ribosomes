function x_ss = RFM_Steady_State(N_r, RFM, options)
    %Compute the steady state number of ribosomes in each codon and the
    %pool. Take advantage of the stochiometric constraint.
    
    if nargin < 3
        %options = [];
        %options = ['Display', 'off'];
        options = optimoptions('fsolve', 'Display', 'off');
    end
    
    %find number of codons    
    N = 0;
    for i = 1:length(RFM)
        N = N + RFM{i}.N;
    end
    
    %anonymous functions
    %f = @(x) RFM_Stoch(x, N_r, RFM);
    %f = @(x) RFM_Standard(0, x, RFM);
    %f =  @(x) RFM_Standard(0, [N_r - sum(x); x], RFM)(2:end);
    
    f = @(x) RFM_Objective(x, N_r, RFM);
    
    
%      x0 = zeros(N+1, 1);
%      x0(1) = N_r;
    %x0 = N_r/(N+1) * ones(N+1, 1);
    x0 = zeros(N, 1);
    
    x_ss_x = fsolve(f, x0, options);
    x_ss = [N_r - sum(x_ss_x); x_ss_x];
end