function xdot = RFM_Objective(x, N_r, RFM)
    %objective function for RFM_Steady_State
    zxdot = RFM_Standard(0, [N_r - sum(x); x], RFM);
    xdot = zxdot(2:end);
end