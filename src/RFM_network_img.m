%general setup
rfm = RFM_Network();
%N_r = 3.25;
N_r = 5;
%N_r = 4;
%N_r = 6;
%t_max = 8;
t_max = 60;

%add rna to system
N1 = 5;
lambda1 = 5*ones(N1+1, 1);
% lambda1(4) = 0.05;
% lambda1(1) = 40;
lambda1(3) = 40;
rfm = rfm.add_rna(1, N1, lambda1);

N2 = 5;
lambda2 = 5*ones(N2+1, 1);
%lambda2 = 5*ones(N2+1, 1);
rfm = rfm.add_rna(1, N2, lambda2);
% 
N3 = 5;
lambda3 = 5*ones(N3+1, 1);
%lambda3(2) = 1;
rfm = rfm.add_rna(1, N3, lambda3);


%get the party started
rfm = rfm.simulate(N_r, 10);

x_ss = rfm.find_steady_state(N_r);
y_ss = rfm.output(x_ss);
%try the visualizer
v = RFM_Visualizer(rfm);

v.draw(t_max, x_ss)