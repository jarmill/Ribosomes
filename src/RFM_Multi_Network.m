classdef RFM_Multi_Network
    %RFM_MULTI_NETWORK Models a ribosomal flow network with a pool.
    %ribosomes are split into N translating pools, plus one empty pool
    %(orthogonal). 
    %   Solves the system
    
    properties
        %structs
        Pool;                %properties of the pool (transition rates for tagging)
        RFM;            %strands of rna in the network (on host)
        %RFM_circ;            %strands of rna in the network (on circuit)
        Inhibitor;           %protein that inhibits expression of the orthogonal ribosome
        N_species;      %number of ribosomal species
        N_strands;
        N_states;       %number of states (pools + codons)
                
        mult;           %multiplicities of codons
        
        traj;           %trajectory of solutions and information
        %simple_model;       %reduced (two-tank) model or full (three-tank) model
        %information about the translation rates
        %entry_codons_host;   %index of first codon on a ribosome
        %exit_codons_host;    %index of last  codon on a ribosome
        %exit_lambda_host;    %translation rate on a ribosome
        %length_codon_host;   %number of codons on each rna
        entry_codons;   %index of first codon on a ribosome
        exit_codons;    %index of last  codon on a ribosome
        exit_lambda;    %translation rate on a ribosome
        length_codon;   %number of codons on each rna
        
       
        
        %entry_codons_circ;   %index of first codon on a ribosome
        %exit_codons_circ;    %index of last  codon on a ribosome
        %exit_lambda_circ;    %translation rate on a ribosome
        %length_codon_circ;   %number of codons on each rna
        
    end
    
    methods
        function obj = RFM_Multi_Network(N_species)
            %constructor

            obj.N_species = N_species;           
            obj.N_states = N_species + 1;
            obj.N_strands = zeros(N_species, 1);
            
            obj.RFM = cell(N_species, 1);   
            obj.mult = cell(N_species, 1);
            
            obj.Pool = struct;            
            
            rate0  = 0.1*ones(N_species, 1);            
            obj = obj.pool_rates(rate0, rate0);
            
            
            obj.Inhibitor = cell(N_species, 1);
            
            %trajectory information
            obj.entry_codons = cell(N_species, 1);
            obj.exit_codons  = cell(N_species, 1);
            obj.exit_lambda  = cell(N_species, 1);
            obj.length_codon = cell(N_species, 1);
            
        end
        
        function obj = pool_rates(obj, tag_rate, untag_rate)
            %set the rates of tagging in the pool
            obj.Pool.tag_rate   = tag_rate;
            obj.Pool.untag_rate = untag_rate;
            
            % steady state proportions
%             split_mat = [diag(obj.Pool.untag_rate) -obj.Pool.tag_rate;
%                             ones(1, obj.N_species+1)];
            split_mat = [ones(1, obj.N_species+1); 
                        -obj.Pool.tag_rate, diag(obj.Pool.untag_rate)];
                         
            obj.Pool.ss = split_mat \ [1; zeros(obj.N_species, 1)];  
            
        end
        
%         function obj = add_inhibitor(obj, p0, kf, delta)
%             %properties of the inhibitor
%             %Input:
%             %   p0:     reference number of protein
%             %   kf:     Hill coefficient
%             %   delta:  degradatioon rate
%             
%             obj.Inhibitor = struct;
%             obj.Inhibitor.p0 = p0; 
%             obj.Inhibitor.kf = kf;
%             obj.Inhibitor.delta = delta;
%             
%             %add a new state for the protein inhibitor
%             obj.N_states = obj.N_states+1;
%             
%             %the first dna strand on the circuit is the controller,
%             %produces the protein inhibitor
%         end
        
        function obj = add_rna(obj, mult, lambda, source, sat_func)
            %adds a strand of RNA to the network
            %Input:
            %   N:      number of codons on the rna strand
            %   lambda: transition rates of rna (translation, elongation,
            %           initiation)
            %   source: from which pool are the ribosomes taken
                        
            if nargin < 4
                source = 1;
            end
            
            if nargin < 5
                sat_func = @(x) tanh(x);
            end
            
            rna = struct;
            N = size(lambda, 1)-1;            
            rna.N = N;
            
            rna.mult = mult;
            obj.mult{source}(end+1) = mult;
            
            
            
            rna.lambda = lambda;
            rna.G = sat_func;
            num_pools = obj.N_species + 1;
                            
            %add to the rfm            
            obj.RFM{source}{end+1} = rna;
            obj.N_strands(source) = obj.N_strands(source) + 1;

            %update trackers for host

            obj.exit_lambda{source}(end+1) = lambda(end);
            obj.entry_codons{source}(end+1) = num_pools  + sum([obj.length_codon{1:source}]) + 1;
            obj.exit_codons{source}(end+1) = num_pools + sum([obj.length_codon{1:source}]) + N;
            obj.length_codon{source}(end+1) = N;              
            
            for i = (source+1):obj.N_species
                obj.entry_codons{i} = obj.entry_codons{i} + N;
                obj.exit_codons{i}  = obj.exit_codons{i} + N;
            end
            
            obj.N_states = obj.N_states + N;            
        end
        
        function [inflow, outflow, xdot] = rna_flow(obj, z, x, rna)
            %RNA_FLOW: finds the change in ribosome content in a single strand
            %
            %Input:
            %   z:          Number of ribosomes in pool
            %   x:          Number of ribosomes in each codon
            %   rna:     	current strand of rna
            %
            %Output:
            %   inflow:     Number of ribosomes flowing in to rna (from pool)
            %   outflow:    Number of ribosomes exiting rna (translation rate)
            %   xdot:       Change in ribosomes in all codons for rna 

            %r_in = 1;
            r_in = rna.G(z);

            x_stagger = [r_in; x; 0];
            x_transfer = rna.lambda .* x_stagger(1:end-1) .* (1-x_stagger(2:end));

            x_inflow  = x_transfer(1:end-1);
            x_outflow = x_transfer(2:end);

            %return output
            inflow = x_transfer(1);
            outflow = x_transfer(end);
            xdot = x_inflow - x_outflow;               
        end
        
        function xdot = dynamics_standard(obj, t, x, RFM)
            %function to perform derivatives
            %xdot = (1:length(x))';

            %prepare for first iteration
            z = x(1);
            ind_curr = 2;
            xdot = [];
            zdot = 0;
            for i = 1:length(RFM)
                rna_curr = RFM{i};        
                ind_next = ind_curr + rna_curr.N - 1;
                x_curr = x(ind_curr:ind_next);

                [inflow, outflow, x_curr_dot] = obj.rna_flow(z, x_curr, rna_curr);

                %prepare for next iteration
                zdot = zdot + rna_curr.mult*(outflow - inflow);
                ind_curr = ind_next + 1;   
                xdot = [xdot; x_curr_dot];
            end

            xdot = [zdot; xdot];

        end
        
        function xdot = objective_standard(obj, x, N_r, RFM)
            %objective function for RFM_Steady_State
            zxdot = obj.dynamics_standard(0, [N_r - sum(x); x], RFM);
            xdot = zxdot(2:end);
        end
        
        function xdot = dynamics_multi(obj, t, x, Pool, RFM) %Inhibitor
            %function to perform derivatives over split ribosome network
            
            %extract properties from x            
            z_empty = x(1);
            z = x(1 + (1:obj.N_species));
            
            xdot = cell(obj.N_species, 1);
            zdot = zeros(obj.N_species, 1);
            
            count = obj.N_species+1;
            xdot_cell = cell(obj.N_species, 1);
            for i = 1:obj.N_species
                %current species pool and ribosomes
                z_curr = z(i);
                x_curr = x(obj.entry_codons{i}(1):obj.exit_codons{i}(end));
                network = [z_curr; x_curr];
                
                %run the derivatives
                xdot_network = obj.dynamics_standard(t, network, RFM{i});
                zdot(i) = xdot_network(1);
                xdot_cell{i} = xdot_network(2:end);
            end            
            
%             %inhibitor
%             if isempty(Inhibitor)
%                 p_f = 0;
%                 p0 = 1;
%                 kf = 1;
%             else
%                 p_f = x(end); %inhibitor proteins
%                 p0 = Inhibitor.p0;
%                 kf = Inhibitor.kf;
%                 delta = Inhibitor.delta;
%             end
%             

            
            %mass action to find rate changes among tagging in pools 
            %hill_inhibition = (1/(1 + (p_f/p0))^kf);
            zdot_empty = Pool.untag_rate'*z - z_empty * sum(Pool.tag_rate);
            zdot = z_empty*Pool.tag_rate - Pool.untag_rate.*z + zdot;

            z_block = [zdot_empty; zdot];
%                 zdot_empty = z_host * Pool.host_untag_rate + z_circ * Pool.circ_untag_rate ...
%                          - z_empty * (Pool.host_tag_rate + Pool.circ_tag_rate * hill_inhibition);
%                 zdot_host = zdot_host + z_empty * Pool.host_tag_rate - z_host * Pool.host_untag_rate;
%                 zdot_circ = zdot_circ + z_empty * Pool.circ_tag_rate * hill_inhibition - z_circ * Pool.circ_untag_rate;
%                 z_block = [zdot_empty; zdot_host; zdot_circ];            
            %end
            
            %output is concatenation of all the stuff
            %if isempty(Inhibitor)
            %    p_f_dot = [];
            %else
%                 p_f_in = x(obj.exit_codons_circ(1)) * obj.exit_lambda_circ(1);
%                 p_f_out = delta*p_f;
%                 p_f_dot = p_f_in - p_f_out;
%             end
%             xdot = [z_block; xdot_host; xdot_circ; p_f_dot];

            xdot = [z_block; cell2mat(xdot_cell)];
        end
        
        
       function [e_ss, R] = rna_steady_state(~, RFM, gz)
            %steady state of single RFM given pool input G(z)
            %Input:
            %   RFM:    Strand of RNA
            %   gz:     G(z), saturation function applied to the pool
            %
            %Output:
            %   e_ss:   Steady state occupancy of codons x
            %   R:      Translation rate of protein at steady state
            
            if nargin < 3
                gz = 1;
            end
            
            
            lam = RFM.lambda;
            lam(1) = lam(1) * gz; %multiply initiation rate by pool saturation function
            
            [m, i] = min(lam);
            
            %if gz == 0
            if m == 0
                %pool empty, nothing coming in
                %practically never happens, but this is just in case
                R = 0;
                e_ss = zeros(size(lam(2:end)));
                
                %bottleneck in the rfm. Will need to fix this and take into
                %account N_r. 
                if i > 1
                    e_ss(1:(i-1)) = 1;
                end
                
            else
                %pool has ribosomes
                
                %form the jacobi matrix
                lam_rsq = 1./sqrt(lam);

                J = diag(lam_rsq, 1) + diag(lam_rsq, -1);

                %extract the perron eigenvector/eigenvalue of J

                %symmetric matrix means all eigenvalues are real
                [zeta, sigma] = eigs(J, 1, 'largestreal');

                %optimal flow rate of output            
                %from spectral representation
                R = 1/(sigma^2);

                %e_ss = lam_rsq/sigma .* zeta(2:end)./zeta(1:end-1);
                e_ss = lam_rsq(2:end)/sigma .* zeta(3:end)./zeta(2:end-1);
            end
            
        end

        
        function [N_r, x_ss] = network_steady_state(obj, z, ss)
            %objective function, determines the total ribosomal occupancy
            %in the network
                                    
            if nargin < 3
                 ss = obj.Pool.ss;
            end
            
            %find the pool splitting steady state            
            z_split = z * ss;
            x_ss = z_split;
            
            N_r = z;
            
            for s = 1:obj.N_species
                RFM_curr = obj.RFM{s};
                for i = 1:length(obj.RFM{s})
                    rna_curr = RFM_curr{i};        
                    gz = rna_curr.G(z_split(s+1));

                    e_ss_curr = obj.rna_steady_state(rna_curr, gz);
                    N_curr = obj.mult{s}(i)* sum(e_ss_curr);

                    x_ss = [x_ss; e_ss_curr];

                    N_r = N_r + N_curr;
                end
            end

            
        end                   
        
        function y_out = get_translation_rate(obj, x_in)
            %find translation rates at a particular x
            
            y_host = obj.exit_lambda_host' .* x_in(obj.exit_codons_host);
            y_circ = obj.exit_lambda_circ' .* x_in(obj.exit_codons_circ);
            
            y_out = [y_host; y_circ];
            
        end
        
        function x_ss = find_steady_state(obj, N_r, ss)
            %use bisection to find optimal steady state
            err_min = 1e-6;
            err = Inf;
            a = 0;
            b = N_r;                                                            
            
            while abs(err) > err_min
                z_curr = (a+b)/2;
                
                %[zh; zc; ze];

                if nargin < 3
                    [N_curr, x_ss] = network_steady_state(obj, z_curr);
                else
                    [N_curr, x_ss] = network_steady_state(obj, z_curr, ss);
                end
                
                err = N_curr - N_r;
                if err > 0
                    b = z_curr;
                else
                    a = z_curr;
                end
            end            
        end        
        
        function  y = output(obj, x)
            exit_ind =  cat (2,obj.exit_codons{:});
            mult = cat(2, obj.mult{:});
            exit_lambda = cat(2, obj.exit_lambda{:});
            y = (mult.*exit_lambda)' .* x(exit_ind);
        end
        
        function y = steady_state_output(obj, w, N_r, arg, use_K, log_K)
           %objective for optimize_output
           %sum of w_i y_i, where y_i is the output of strand i and w_i is
           %the weight of strand i
           %
           %Input:
           %    w:      weights of output: y = sum w_i y_i
           %    N_r:    number of ribosomes in system
           %    arg:    either K or the steady state
           %    use_K:  is arg K (true) or the steady state (false)
           %    log_K:  log10(K) (true) or K (false)
           %
           %Output:
           %    y:  	weighted total protein output
           
           if nargin <= 4 || use_K==0
               %arg is steady state
               ss = arg;               
           else
               %arg is current K
               if nargin ==6 && log_K
                   ss = [1; 10.^(arg)]./(1 + sum(10.^(arg)));
               else
                   ss = [1; arg]./(1 +sum(arg));
               end
           end
           
           if any(isnan(arg))
                y = NaN;
           else
                x_ss = obj.find_steady_state(N_r, ss);
                y_ss = obj.output(x_ss);
                y  = w'*y_ss;
           end
        end
        
         
        function [f_opt, K_opt, x_ss_opt] = optimize_output_range(obj, w, N_r, k_tag, k_untag)
            %optimize the weighted output with respect to equilibrium
            %rate constants k_tag/k_untag = K
            
            %process constraint bounds
            if nargin <= 4
                lb = k_tag(:, 1);
                ub = k_tag(:, 2);
            else
                %K_range = zeros(size(k_tag));
                
                %low
                lb = k_tag(:, 1)./k_untag(:, 2);
                %high
                ub = k_tag(:, 2)./k_untag(:, 1);
            end
            
            
            %logarithm
            log_K = 0;
            
            f = @(K) -steady_state_output(obj, w, N_r, K, 1, log_K);
            
            %x0 = (lb+ub)/2;
            if log_K
                lb = log10(lb);
                ub = log10(ub);
            end
            x0 = (lb + ub)/2;
            %x0 = ub - 1e-4;
            options = optimoptions('fmincon','Display','iter','Algorithm','sqp');
            [K_opt, f_opt, exitflag, output] = fmincon(f,x0,[],[],[], [], lb,ub,[], options);
            f_opt = -f_opt;
            
            if log_K
                K_opt_log = K_opt;
                K_opt = 10.^(K_opt);
            end
                
            ss = [1; K_opt]/(1+sum(K_opt));
            x_ss_opt = obj.find_steady_state(N_r, ss);
            
                
            %r_opt = [r_part_opt; 1-epsilon - sum(r_part_opt)];
               
        end
        
        function [f_opt, r_opt] = optimize_output_epsilon(obj, w, N_r, epsilon)
            %optimize the weighted output with respect to equilibrium
            %number of ribosomes in the pools
            
            %Input:
            %   w:       weights of protein output 
            %   N_r:     number of ribosomes in network
            %   epsilon: proportion of ribosomes in empty pool vs. all
            %            pools at steady state
            %  
            %Output:
            %   y_opt:   optimum weighted protein output
            %   r_opt:   optimum proportion of ribosomes in pools
            
            
            %box constraints
            lb = zeros(obj.N_species-1, 1);
            ub = (1-epsilon)*ones(obj.N_species-1, 1);
            
            %sum of proportions
            A = ones(1, obj.N_species-1);
            b = 1-epsilon;
            
            %on probability simplex: epsilon + sum_r = 1
            %so kill the last coordinate
            f = @(r_part) -steady_state_output(obj, w, N_r, [epsilon; r_part; 1-epsilon - sum(r_part)], 0);
            
            x0 = ones(obj.N_species-1, 1)/obj.N_species;
            
            [r_part_opt, f_opt] = fmincon(f,x0,A,b,[], [], lb,ub);
            f_opt = -f_opt;
            
            r_opt = [r_part_opt; 1-epsilon - sum(r_part_opt)];
            
        end
        
        function [tag_rates, untag_rates] = recover_rates(obj, epsilon, r)
           untag_rates = ones(size(r));
           tag_rates = r./epsilon;
        end
        
        function obj = simulate(obj, N_r, t_max)
            %solve the differential equation in dynamics_standard
            
            traj_out = struct;
            traj_out.N_r = sum(N_r);
            
            t_range = [0 t_max];
            x0 = zeros(obj.N_states, 1);
            if length(N_r) == obj.N_species
                %put ribosomes in the host/circuit pools
                x0((1:obj.N_species) + 1) = N_r;
            else
                %put ribosomes in the empty pool
                x0(1) = N_r;
            end
            
            %find the trajectory of motion
            [T, X] = ode45(@obj.dynamics_multi, t_range, x0, [], obj.Pool, obj.RFM);
            
            traj_out.t = T;
            traj_out.x = X;  
            
            traj_out.y = cell(obj.N_species, 1);
            for i = 1:obj.N_species
                traj_out.y{i} = X(:, obj.exit_codons{i})*diag(obj.exit_lambda{i});
            end
            %traj_out.y_host = X(:, obj.exit_codons_host) * diag(obj.exit_lambda_host);            
            %traj_out.y_circ = X(:, obj.exit_codons_circ) * diag(obj.exit_lambda_circ);
            
            %traj_out.y = X(:, obj.exit_codons) * diag(obj.exit_lambda);
            
%             %steady state solutions
%             x_ss = obj.steady_state(sum(N_r));
%             y_host_ss = obj.exit_lambda_host' .* x_ss(obj.exit_codons_host);
%             y_circ_ss = obj.exit_lambda_circ' .* x_ss(obj.exit_codons_circ);
% 
%             traj_out.x_ss = x_ss;
%             traj_out.y_host_ss = y_host_ss;            
%             traj_out.y_circ_ss = y_circ_ss;
             traj_out.spline = spline(T, X');
            
            obj.traj = traj_out;
            
        end       

    end
end

