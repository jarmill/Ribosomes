function xdot = RFM_Standard(t, x, RFM)
    %function to perform derivatives
    %xdot = (1:length(x))';
    
    %prepare for first iteration
    z = x(1);
    ind_curr = 2;
    xdot = [];
    zdot = 0;
    for i = 1:length(RFM)
        rna_curr = RFM{i};        
        ind_next = ind_curr + rna_curr.N - 1;
        x_curr = x(ind_curr:ind_next);

        [inflow, outflow, x_curr_dot] = RFM_Flow(z, x_curr, rna_curr.lambda);
        
        %prepare for next iteration
        zdot = zdot + outflow - inflow;
        ind_curr = ind_next + 1;   
        xdot = [xdot; x_curr_dot];
    end
    
    xdot = [zdot; xdot];

end
