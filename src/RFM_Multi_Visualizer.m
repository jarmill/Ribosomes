classdef RFM_Multi_Visualizer
% Implements the draw function for the Flow Engine model
% based on heave dynamics of an air cushioned vehicle

    properties
        %trajectory parameters
        sample_rate;
        traj;
        RFM
        Inhibitor;
        Pool;
        
        %indexing
        entry_codons;
        exit_codons;
        length_codons;
        max_codon;
        mult;
        
        %visualization parameters
        w_codon = 0.1;
        h_codon = 0.1;
        offset_codon = 0.2;
        offset_rna = 0.2;
        axis_limit;
        N_species;
        title = 1;
        
        sum_radius = 0.05;
        
        width_mult = 4;
        
        w_pool = 0.4;
        h_pool = 0.8;
        pool_shift = -0.5;
        pool_shift_vert = 1.5;
        pool_drop = 1.4;
    end

    methods
        function obj = RFM_Multi_Visualizer(plant, sample_rate)
            obj.RFM = plant.RFM;
            
            obj.Inhibitor = plant.Inhibitor;
            obj.Pool = plant.Pool;
            obj.traj = plant.traj;
            obj.pool_shift_vert = 1.5*obj.h_pool;
            obj.mult = plant.mult;
            
            obj.N_species = plant.N_species;
            %shortcuts here
            obj.entry_codons = plant.entry_codons;            
            obj.exit_codons = plant.exit_codons;
            obj.length_codons = plant.length_codon; 
            obj.max_codon = max(cellfun(@max, obj.length_codons));
            
            %obj.axis_limit = [-0.7 (obj.offset_codon*max([obj.length_codon_host obj.length_codon_circ]) + 0.3) -0.8-obj.pool_shift_vert 0.8];
            obj.axis_limit = [-1.5 (obj.offset_codon*obj.max_codon + 0.3) -0.8-obj.pool_shift_vert*(obj.N_species-1) 0.8];
            
            if nargin < 2
                obj.sample_rate = 0.05;
            else
                obj.sample_rate = sample_rate;
            end
        end
    
        function draw(obj,t,x, w)
            % Draw the Ribosomal Flow Network
            %       persistent hFig skate bag;
            persistent hFig codon pool circ;
            
            


            C = linspecer(obj.N_species);
            if (isempty(hFig) || ~isvalid(hFig))
                hFig = figure;
                set(hFig,'DoubleBuffer', 'on');
                
                codon = [obj.w_codon/2*[1 -1 -1 1]; obj.h_codon/2*[1 1 -1 -1]];
                pool = [obj.w_pool/2*[1 -1 -1 1]; obj.h_pool/2*[1 1 -1 -1]];
                th = linspace(0, 2*pi, 150);
                circ = [cos(th); sin(th)];
            end
            
            figure(hFig); cla; hold on; view(0,90);

            %extract data            
            z_empty = x(1);
            z = x(1 + (1:obj.N_species));
            
            empty_to_trans = z_empty * obj.Pool.tag_rate;
            trans_to_empty = z  .* obj.Pool.untag_rate;
            
            
            pool_edge = obj.w_pool/2 - 0.5;
            empty_pool = -(obj.N_species -1)*obj.pool_shift_vert/2;
            
            port_x = linspace(0.2, 0.8, floor(obj.N_species/2)*2)*obj.w_pool - obj.w_pool/2 + 2.25*obj.pool_shift;
            
            if mod(obj.N_species, 2) == 0
                port_x_empty_trans  = [port_x(1:2:end-1), port_x(end:-2:2)];
                port_x_trans_empty  = [port_x(2:2:end), port_x(end-1:-2:1)];
            else
                port_x_empty_trans  = [port_x(1:2:end-1), 2.25*obj.pool_shift ,port_x(end:-2:2)];               
                %port_x_trans_empty  = [port_x(2:2:end), 2.25*obj.pool_shift, port_x(end:-2:2)];
                port_x_trans_empty  = [port_x(2:2:end), 2.25*obj.pool_shift, port_x(1:2:end-1)];
            end
            
            %% draw the RNA (host)
            y = [];
            N_r = sum(x(1:(obj.N_species+1)));
            for s = 1:obj.N_species
                N_strand = length(obj.RFM{s});
            
                %handle the column offsets
                possible_col = linspace(obj.h_pool/2, -obj.h_pool/2, N_strand + 2);
                col_list = possible_col(2:end-1);

                outflow = 0;
                curr_color = C(s, :);
                pool_shift = obj.pool_shift_vert*(s-1);                
                for r = 1:N_strand
                    %get the current states along the RNA                                                            
                    species_shift = -obj.pool_shift_vert*(s-1);
                    offset_col = col_list(r) - obj.pool_shift_vert*(s-1);
                    rna_curr = obj.RFM{s}{r};
                    x_curr = x(obj.entry_codons{s}(r) : obj.exit_codons{s}(r));
                    N_r = N_r + obj.mult{s}(r)*sum(x_curr);
                    
                    %find the flow 
                    r_in = rna_curr.G(z(s));
                    x_stagger = [r_in; x_curr; 0];
                    x_transfer = rna_curr.lambda .* x_stagger(1:end-1) .* (1-x_stagger(2:end));

                    %summing junction setup, centered at (summer_x, 0)
                    summer_x = (obj.offset_codon*max(obj.length_codons{s}) + 3*obj.sum_radius);

                    %transfer from pool
                    if x_transfer(1) > 0
                        line([pool_edge; -obj.w_codon/2], [offset_col; offset_col], 'LineWidth', obj.width_mult*x_transfer(1), 'Color', curr_color);
                    end



                    for i = 1:rna_curr.N
                        %plot position of each codon
                        offset_row = obj.offset_codon*(i-1);
                        if x_transfer(i+1) > 0
                            if i == rna_curr.N
                                %blah
                                %line(obj.offset_codon*0.5 + offset_row +obj.w_codon/2*[-1, 1], [offset_col; offset_col], 'LineWidth', obj.width_mult*x_transfer(i+1), 'Color', [78,206,255]/255);
                                %connection to kink in pipe
                                line([(obj.offset_codon*0.5 + offset_row -obj.w_codon/2); (obj.offset_codon*max(obj.length_codons{s})); summer_x], [offset_col; offset_col; species_shift], 'LineWidth', obj.width_mult*x_transfer(i+1), 'Color', curr_color);
%                                 text( (obj.offset_codon*max(obj.length_codons{s}) + 2*obj.w_codon), offset_col + 0.05, ['y^{', num2str(s), ',' num2str(r), '}=', num2str(x_transfer(i+1), 2), ' x', num2str(obj.mult{s}(r))]);
                                text( (obj.offset_codon*max(obj.length_codons{s}) + 2*obj.w_codon), ...
                                    offset_col + 0.05, ['y^{', num2str(s), ',' num2str(r), '}=', num2str(x_transfer(i+1), 2)]);
                                y(end+1) = x_transfer(i+1)*obj.mult{s}(r);
                                %obj.offset_codon* - 0.6*obj.offset_codon, offset_col + 0.05,

                            else
                                line(obj.offset_codon*0.5 + offset_row +obj.w_codon/2*[-1, 1], [offset_col; offset_col], 'LineWidth', obj.width_mult*x_transfer(i+1), 'Color', curr_color);
                            end
                        end
                        %patch(codon(1, :) + offset_row, x_curr(i)*codon(2, :) - obj.h_codon/2*(1-x_curr(i)) + offset_col, 'b', 'EdgeColor', 'None')
                        patch(codon(1, :) + offset_row, x_curr(i)*codon(2, :) - obj.h_codon/2*(1-x_curr(i)) + offset_col , 0, 'FaceColor', curr_color, 'EdgeColor', 'none')
                        patch(codon(1, :) + offset_row, codon(2, :) + offset_col, 'k', 'FaceColor', 'None', 'LineWidth', 2)                


                    end
                    %ribosomes returning to pool
                    outflow = outflow + x_transfer(i+1);

                    %ind = ind+rna_curr.N;
                end

                %pipes for the summer junction

                %draw the summer junction
                if outflow > 0
                    line([summer_x; summer_x; obj.pool_shift; obj.pool_shift], -obj.h_pool/2 * [0; obj.pool_drop; obj.pool_drop; 1] + species_shift, 'LineWidth', obj.width_mult*outflow, 'Color', curr_color);

                end
                patch(summer_x + obj.sum_radius * circ(1, :), obj.sum_radius * circ(2, :) + species_shift, 'k', 'FaceColor', [1, 1, 1], 'LineWidth', 1.75)
                line(summer_x + 0.6*obj.sum_radius*[-1; 1], [0; 0] + species_shift, 'Color', 'k', 'LineWidth', 1.75)
                line(summer_x * [1; 1], 0.6*obj.sum_radius*[-1; 1]+ species_shift, 'Color', 'k', 'LineWidth', 1.75)
                
                %draw links to the pool
                %port_x_empty_trans;
                %port_x_trans_empty;
                if empty_to_trans(s)
                     line([port_x_empty_trans(s), port_x_empty_trans(s), obj.pool_shift - obj.w_pool/2],...
                     [empty_pool + obj.h_pool/2, species_shift + 0.6*obj.h_pool/2, species_shift + 0.6*obj.h_pool/2], ...
                     'Color', [216,191,216]/255, 'LineWidth', obj.width_mult*empty_to_trans(s))
                end
                
                if trans_to_empty(s)
                     line([port_x_trans_empty(s), port_x_trans_empty(s), obj.pool_shift - obj.w_pool/2],...
                     [empty_pool + obj.h_pool/2, species_shift - 0.4*obj.h_pool/2, species_shift - 0.4*obj.h_pool/2], ...
                     'Color', curr_color, 'LineWidth', obj.width_mult*trans_to_empty(s))
                end                                                
            end
            
            %draw the pool
            for s = 1:obj.N_species
                species_shift = -obj.pool_shift_vert*(s-1);
                z_scaled = z(s)/N_r;
                patch(pool(1, :) + obj.pool_shift, z_scaled*pool(2, :) - obj.h_pool/2*(1-z_scaled) + species_shift, 0, 'FaceColor', C(s, :), 'EdgeColor', 'None')
                patch(pool(1, :) + obj.pool_shift, pool(2, :) + species_shift, 'k', 'FaceColor', 'None', 'LineWidth', 3)                
            end
                        
        %% empty pool section
             z_empty_scaled = z(s)/N_r;
             patch(pool(1, :) + 2.25*obj.pool_shift, pool(2, :) + empty_pool, 'w', 'EdgeColor', 'none')  

             patch(pool(1, :) + 2.25*obj.pool_shift, z_empty_scaled*pool(2, :) - obj.h_pool/2*(1-z_empty_scaled) + empty_pool, [148,0,211]/255, 'EdgeColor', 'None')
 
             patch(pool(1, :) + 2.25*obj.pool_shift, pool(2, :) + empty_pool, 'k', 'FaceColor', 'None', 'LineWidth', 3)  

        

            %empty_to_trans = z_empty * obj.Pool.tag_rate;
            %trans_to_empty = z  .* obj.Pool.untag_rate;
            
%             %host connections
%             if empty_to_host > 0
%                 line([2.25*obj.pool_shift - 0.4*obj.w_pool/2, 2.25*obj.pool_shift- 0.4*obj.w_pool/2, obj.pool_shift - obj.w_pool/2],...
%                     [-obj.pool_shift_vert/2 + obj.h_pool/2, 0.6*obj.h_pool/2, 0.6*obj.h_pool/2], 'Color', [216,191,216]/255, 'LineWidth', obj.width_mult*empty_to_host)
%             end
% 
%             if trans_to_empty> 0
%                 line([2.25*obj.pool_shift + 0.4*obj.w_pool/2, 2.25*obj.pool_shift + 0.4*obj.w_pool/2, obj.pool_shift - obj.w_pool/2],...
%                     [-obj.pool_shift_vert/2 + obj.h_pool/2, 0, 0], 'Color', [78,206,255]/255, 'LineWidth', obj.width_mult*host_to_empty)
% 
%                  line([2.25*obj.pool_shift + 0.4*obj.w_pool/2, 2.25*obj.pool_shift + 0.4*obj.w_pool/2, obj.pool_shift - obj.w_pool/2],...
%                     [-obj.pool_shift_vert/2 - obj.h_pool/2,  - obj.pool_shift_vert, - obj.pool_shift_vert], 'Color', [247,120,120]/255, 'LineWidth', obj.width_mult*circ_to_empty)
% 
%             end

%             %circuit connections
%             if empty_to_circ > 0
%                 if ~isempty(obj.Inhibitor)
%                     hill_inhibition = (1/(1 + (x(end)/obj.Inhibitor.p0))^obj.Inhibitor.kf);
% 
%                     line([2.25*obj.pool_shift - 0.4*obj.w_pool/2, 2.25*obj.pool_shift- 0.4*obj.w_pool/2, obj.pool_shift - obj.w_pool/2],...
%                     [-obj.pool_shift_vert/2 - obj.h_pool/2, -0.6*obj.h_pool/2 - obj.pool_shift_vert, -0.6*obj.h_pool/2 - obj.pool_shift_vert], 'Color', [238,232,170]/255, 'LineWidth', obj.width_mult*empty_to_circ)
% 
%                     line([2.25*obj.pool_shift - 0.4*obj.w_pool/2, 2.25*obj.pool_shift- 0.4*obj.w_pool/2, obj.pool_shift - obj.w_pool/2],...
%                      [-obj.pool_shift_vert/2 - obj.h_pool/2, -0.6*obj.h_pool/2 - obj.pool_shift_vert, -0.6*obj.h_pool/2 - obj.pool_shift_vert], 'Color', [216,191,216]/255, 'LineWidth', obj.width_mult*empty_to_circ*hill_inhibition)
%                 else
%                     line([2.25*obj.pool_shift - 0.4*obj.w_pool/2, 2.25*obj.pool_shift- 0.4*obj.w_pool/2, obj.pool_shift - obj.w_pool/2],...
%                     [-obj.pool_shift_vert/2 - obj.h_pool/2, -0.6*obj.h_pool/2 - obj.pool_shift_vert, -0.6*obj.h_pool/2 - obj.pool_shift_vert], 'Color', [216,191,216]/255, 'LineWidth', obj.width_mult*empty_to_circ)
%                 end
% 
%             end
% 
%             if circ_to_empty > 0
%                 line([2.25*obj.pool_shift + 0.4*obj.w_pool/2, 2.25*obj.pool_shift + 0.4*obj.w_pool/2, obj.pool_shift - obj.w_pool/2],...
%                     [-obj.pool_shift_vert/2 - obj.h_pool/2,  - obj.pool_shift_vert, - obj.pool_shift_vert], 'Color', [247,120,120]/255, 'LineWidth', obj.width_mult*circ_to_empty)
%             end



% 
%             z_host_scaled = z_host/sum(N_r);
%             patch(pool(1, :) + obj.pool_shift, z_host_scaled*pool(2, :) - obj.h_pool/2*(1-z_host_scaled), 'b', 'EdgeColor', 'None')
%             patch(pool(1, :) + obj.pool_shift, pool(2, :), 'k', 'FaceColor', 'None', 'LineWidth', 3)                
% 
% 
%             z_circ_scaled = z_circ/sum(N_r);
%             patch(pool(1, :) + obj.pool_shift, z_circ_scaled*pool(2, :) - obj.h_pool/2*(1-z_circ_scaled) - obj.pool_shift_vert, 'r', 'EdgeColor', 'None')
%             patch(pool(1, :) + obj.pool_shift, pool(2, :) - obj.pool_shift_vert, 'k', 'FaceColor', 'None', 'LineWidth', 3)  
% 
%             z_empty_scaled = z_empty/sum(N_r);
%             patch(pool(1, :) + 2.25*obj.pool_shift, z_empty_scaled*pool(2, :) - obj.h_pool/2*(1-z_empty_scaled) - obj.pool_shift_vert/2, [148,0,211]/255, 'EdgeColor', 'None')
% 
%             patch(pool(1, :) + 2.25*obj.pool_shift, pool(2, :) - obj.pool_shift_vert/2, 'k', 'FaceColor', 'None', 'LineWidth', 3)  

            %done with plotting
            axis image; 
            axis(obj.axis_limit);
            %scatter(cos(t), sin(t))
            %title(['t = ', num2str(t(1),'%.2f') ' sec']);
            
            if nargin > 3
                y_out  = y*w;
                title(['N_r = ', num2str(sum(N_r)), ', t = ', num2str(t(1),'%.2f') ' sec, y= ' ,num2str(y_out,'%.2f')]);
            else
                title(['N_r = ', num2str(sum(N_r)), ', t = ', num2str(t(1),'%.2f') ' sec']);
            end
            
            if ~obj.title
                title('')
            end
            
            set(gca,'XTick',[],'YTick',[])
            set(gca,'XColor','none','YColor','none')

            drawnow;
        end    
        
        function M = playback(obj, filename)
            %run the animation
            
            if (nargin < 2)
                record = 0;
            else
                record = 1;
            end
            
            t_min = obj.traj.spline.breaks(1);
            t_max = obj.traj.spline.breaks(end);
            
            t = t_min:obj.sample_rate:t_max;            
            
            x_interp = ppval(obj.traj.spline, t);
            
            %set up folder
            if record && (exist(['img/', filename], 'dir') == 0)
                mkdir(['img/', filename]);
            end
            
            for i = 1:length(t)
                t_curr = t(i);
                x_curr = x_interp(:, i);
                
                obj.draw(t_curr, x_curr);
                
                %print out the images
                if record
                    print(['img/', filename, '/Frame ' num2str(i)], '-dpng', '-r150');
                end
            end
            
            if record
                %save the output
                GifName = strcat('img/gif_out/', filename, '.gif');
                delay = obj.sample_rate;
                for i = 1:length(t)
                    [A, ~] = imread(['img/', filename, '/Frame ', num2str(i), '.png']);
                    [X, map] = rgb2ind(A, 256);
                    if i == 1
                        imwrite(X, map, GifName, 'gif', 'LoopCount', inf, 'DelayTime', delay)
                    else
                        imwrite(X, map, GifName, 'gif', 'WriteMode', 'append', 'DelayTime', delay)
                    end
                end
            end
        end
    end
  
end
